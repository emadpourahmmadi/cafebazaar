package ir.emad.pourahmadi.cafebazaar.ui.features.fragments.base;


import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public abstract class BaseFragment<B extends ViewDataBinding, T extends ViewModel> extends Fragment {

    String TAG = BaseFragment.class.getSimpleName();
    protected FragmentNavigationAndTitle mFragmentNavigation;

    protected T viewModel;
    protected B binding;

    @Override
    public void onCreate (@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        beforeView();
    }

    @Nullable
    @Override
    public View onCreateView (@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        ViewCompat.setLayoutDirection(binding.getRoot(), ViewCompat.LAYOUT_DIRECTION_LTR);
        binding.setLifecycleOwner(this);
        viewModel = new ViewModelProvider(this).get(getViewModel());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated (@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        afterView();
    }


    @Override
    public void onAttach (Activity activity) {
        getContexts(activity);
        if (activity instanceof FragmentNavigationAndTitle)
            this.mFragmentNavigation = (FragmentNavigationAndTitle) activity;
        super.onAttach(activity);
    }

    @Override
    public void onDestroyView () {
        super.onDestroyView();
        destroyView();
        binding.unbind();
    }

    public abstract void getContexts (Activity activity);

    public abstract @LayoutRes
    int getLayoutId ();

    protected abstract Class <T> getViewModel ();

    protected abstract B getViewBinding ();

    public abstract void beforeView ();

    public abstract void afterView ();


    public abstract void destroyView ();


    public interface FragmentNavigationAndTitle {

        void pushFragment (Fragment fragment);
    }
}