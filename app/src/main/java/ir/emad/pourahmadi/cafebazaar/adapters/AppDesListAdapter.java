package ir.emad.pourahmadi.cafebazaar.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import ir.emad.pourahmadi.cafebazaar.App;
import ir.emad.pourahmadi.cafebazaar.R;
import ir.emad.pourahmadi.cafebazaar.databinding.ItemDescBinding;

public class AppDesListAdapter extends RecyclerView.Adapter <AppDesListAdapter.ContactViewHolder> {
    private final Animation doneAnim;
    String TAG = AppDesListAdapter.class.getSimpleName();
    private static final int VIEW_SELECT = 0;


    private List <String> list = new ArrayList <>();

    public AppDesListAdapter () {
        doneAnim = AnimationUtils.loadAnimation(App.getINSTANCE(),
                android.R.anim.slide_out_right);
        doneAnim.setDuration(600);
    }

    public void setData (List <String> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount () {
        if (list == null)
            return 0;
        return list.size();
    }

    public void clear () {
        list.clear();
    }


    public int getItemViewType (int position) {
        return VIEW_SELECT;
    }


    @SuppressLint({"SetTextI18n", "ClickableViewAccessibility", "UseCompatLoadingForDrawables", "DefaultLocale"})
    @Override
    public void onBindViewHolder (@NonNull final ContactViewHolder holder, final int position) {
        String model = list.get(position);
        ItemDescBinding bindingSelected = holder.bindingSelected;
        bindingSelected.txtTitle.setText(model);
        if (list.size() <= 0 || position == list.size() - 1)
            bindingSelected.View.setVisibility(View.GONE);
    }


    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder (@NonNull ViewGroup viewGroup, int viewType) {
        ViewDataBinding binding;

        switch (viewType) {
            case VIEW_SELECT:
                binding = DataBindingUtil.inflate(LayoutInflater.
                        from(viewGroup.getContext()), R.layout.item_desc, viewGroup, false);
                return new ContactViewHolder((ItemDescBinding) binding);
        }
        return null;
    }


    public class ContactViewHolder extends RecyclerView.ViewHolder {

        private ItemDescBinding bindingSelected;

        ContactViewHolder (ItemDescBinding binding) {
            super(binding.getRoot());
            this.bindingSelected = binding;
        }
    }
}
