package ir.emad.pourahmadi.cafebazaar.data.model.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

import androidx.annotation.NonNull;

@Parcel(Parcel.Serialization.BEAN)
public class Venues {
    @NonNull
    @Expose
    @SerializedName("id")
    private String id;
    @Expose
    @SerializedName("venuePage")
    private VenuePage venuePage;
    @Expose
    @SerializedName("categories")
    private List <Categories> categories;
    @Expose
    @SerializedName("location")
    private Location location;
    @Expose
    @SerializedName("name")
    private String name;


    public VenuePage getVenuePage () {
        return venuePage;
    }

    public void setVenuePage (VenuePage venuePage) {
        this.venuePage = venuePage;
    }


    public List <Categories> getCategories () {
        return categories;
    }

    public void setCategories (List <Categories> categories) {
        this.categories = categories;
    }

    public Location getLocation () {
        return location;
    }

    public void setLocation (Location location) {
        this.location = location;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getId () {
        return id;
    }

    public void setId (String id) {
        this.id = id;
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class VenuePage {
        @Expose
        @SerializedName("id")
        private String id;

        public String getId () {
            return id;
        }

        public void setId (String id) {
            this.id = id;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Categories {
        @Expose
        @SerializedName("primary")
        private boolean primary;
        @Expose
        @SerializedName("icon")
        private Icon icon;
        @Expose
        @SerializedName("shortName")
        private String shortName;
        @Expose
        @SerializedName("pluralName")
        private String pluralName;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private String id;

        public boolean getPrimary () {
            return primary;
        }

        public void setPrimary (boolean primary) {
            this.primary = primary;
        }

        public Icon getIcon () {
            return icon;
        }

        public void setIcon (Icon icon) {
            this.icon = icon;
        }

        public String getShortName () {
            return shortName;
        }

        public void setShortName (String shortName) {
            this.shortName = shortName;
        }

        public String getPluralName () {
            return pluralName;
        }

        public void setPluralName (String pluralName) {
            this.pluralName = pluralName;
        }

        public String getName () {
            return name;
        }

        public void setName (String name) {
            this.name = name;
        }

        public String getId () {
            return id;
        }

        public void setId (String id) {
            this.id = id;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Icon {
        @Expose
        @SerializedName("suffix")
        private String suffix;
        @Expose
        @SerializedName("prefix")
        private String prefix;

        public String getSuffix () {
            return suffix;
        }

        public void setSuffix (String suffix) {
            this.suffix = suffix;
        }

        public String getPrefix () {
            return prefix;
        }

        public void setPrefix (String prefix) {
            this.prefix = prefix;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Location {
        @Expose
        @SerializedName("formattedAddress")
        private List <String> formattedAddress;
        @Expose
        @SerializedName("country")
        private String country;
        @Expose
        @SerializedName("state")
        private String state;
        @Expose
        @SerializedName("city")
        private String city;
        @Expose
        @SerializedName("cc")
        private String cc;
        @Expose
        @SerializedName("postalCode")
        private String postalCode;
        @Expose
        @SerializedName("distance")
        private int distance;
        @Expose
        @SerializedName("lng")
        private double lng;
        @Expose
        @SerializedName("lat")
        private double lat;
        @Expose
        @SerializedName("crossStreet")
        private String crossStreet;
        @Expose
        @SerializedName("address")
        private String address;

        public List <String> getFormattedAddress () {
            return formattedAddress;
        }

        public void setFormattedAddress (List <String> formattedAddress) {
            this.formattedAddress = formattedAddress;
        }

        public String getCountry () {
            return country;
        }

        public void setCountry (String country) {
            this.country = country;
        }

        public String getState () {
            return state;
        }

        public void setState (String state) {
            this.state = state;
        }

        public String getCity () {
            return city;
        }

        public void setCity (String city) {
            this.city = city;
        }

        public String getCc () {
            return cc;
        }

        public void setCc (String cc) {
            this.cc = cc;
        }

        public String getPostalCode () {
            return postalCode;
        }

        public void setPostalCode (String postalCode) {
            this.postalCode = postalCode;
        }

        public int getDistance () {
            return distance;
        }

        public void setDistance (int distance) {
            this.distance = distance;
        }


        public double getLng () {
            return lng;
        }

        public void setLng (double lng) {
            this.lng = lng;
        }

        public double getLat () {
            return lat;
        }

        public void setLat (double lat) {
            this.lat = lat;
        }

        public String getCrossStreet () {
            return crossStreet;
        }

        public void setCrossStreet (String crossStreet) {
            this.crossStreet = crossStreet;
        }

        public String getAddress () {
            return address;
        }

        public void setAddress (String address) {
            this.address = address;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Reasons {
        @Expose
        @SerializedName("items")
        private List <Item> items;
        @Expose
        @SerializedName("count")
        private int count;

        public List <Item> getItems () {
            return items;
        }

        public void setItems (List <Item> items) {
            this.items = items;
        }

        public int getCount () {
            return count;
        }

        public void setCount (int count) {
            this.count = count;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Item {
        @Expose
        @SerializedName("reasonName")
        private String reasonName;
        @Expose
        @SerializedName("type")
        private String type;
        @Expose
        @SerializedName("summary")
        private String summary;

        public String getReasonName () {
            return reasonName;
        }

        public void setReasonName (String reasonName) {
            this.reasonName = reasonName;
        }

        public String getType () {
            return type;
        }

        public void setType (String type) {
            this.type = type;
        }

        public String getSummary () {
            return summary;
        }

        public void setSummary (String summary) {
            this.summary = summary;
        }
    }




}
