package ir.emad.pourahmadi.cafebazaar.db.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.UserLogin;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesItems;

@Database(entities = {UserLogin.class, VenuesItems.class}, version = 2, exportSchema = false)
@TypeConverters({DateConverter.class})

public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;

    public abstract UserDao userDao ();

    public abstract VenuDao venuDao ();

}
