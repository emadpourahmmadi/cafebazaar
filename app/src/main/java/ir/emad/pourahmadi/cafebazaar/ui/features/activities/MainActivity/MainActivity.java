package ir.emad.pourahmadi.cafebazaar.ui.features.activities.MainActivity;

import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.ncapdevi.fragnav.FragNavController;
import com.ncapdevi.fragnav.FragNavTransactionOptions;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import dagger.hilt.android.AndroidEntryPoint;
import ir.emad.pourahmadi.cafebazaar.R;
import ir.emad.pourahmadi.cafebazaar.databinding.ActivityMainBinding;
import ir.emad.pourahmadi.cafebazaar.ui.features.activities.base.BaseActivity;
import ir.emad.pourahmadi.cafebazaar.ui.features.fragments.base.BaseFragment;
import ir.emad.pourahmadi.cafebazaar.ui.features.fragments.home.HomeFragment;
import ir.emad.pourahmadi.cafebazaar.utils.CustomToast.Toasts;
import ir.emad.pourahmadi.cafebazaar.utils.UtilApp;
import ir.emad.pourahmadi.cafebazaar.viewmodel.MainViewModel;

@AndroidEntryPoint
public class MainActivity extends BaseActivity <ActivityMainBinding, MainViewModel>
        implements MainNavigator, FragNavController.RootFragmentListener, BaseFragment.FragmentNavigationAndTitle {
    private String TAG = MainActivity.class.getSimpleName();
    private MainActivity context;
    private FragNavController fragNavController;
    private boolean doubleBackToExitPressedOnce = false;
    protected OnBackPressedListener onBackPressedListener;
    private UUID syncId;

    @Override
    public void bundleSyncId (UUID uuid) {
        syncId = uuid;
    }

    public interface OnBackPressedListener {
        void doBack ();
    }

    public void setOnBackPressedListener (OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

    @Override
    protected Class <MainViewModel> getViewModel () {
        return MainViewModel.class;
    }

    @Override
    protected ActivityMainBinding getViewBinding () {
        return binding;
    }

    @Override
    public void beforeView () {
        context = MainActivity.this;
        viewModel.setNavigator(this);
        binding.setViewModel(viewModel);
    }

    @Override
    protected int getLayout () {
        return R.layout.activity_main;
    }

    @Override
    protected void onResume () {
        super.onResume();
    }

    @Override
    public void afterView (Bundle savedInstanceState) {

        viewModel.getLoading().observe(this, aBoolean -> {
        });

        viewModel.getError().observe(this, s ->
                Toast.makeText(context, s != null ? s + "" : context.getResources().getString(R.string.errorData), Toast.LENGTH_LONG));

        this.fragNavController = new FragNavController(getSupportFragmentManager(), R.id.container);
        this.fragNavController.setRootFragmentListener(this);
        this.fragNavController.setFragmentHideStrategy(FragNavController.HIDE);
        this.fragNavController.initialize(FragNavController.TAB1, savedInstanceState);

        FragNavTransactionOptions.Builder builder = new FragNavTransactionOptions.Builder();
        builder.customAnimations(R.anim.slide_in_right, R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_up).build();

    }

    @Override
    public void destroyView () {
        onBackPressedListener = null;
    }

    @Override
    public void onSaveInstanceState (@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        this.fragNavController.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed () {


        if (fragNavController != null)
            if (fragNavController.isRootFragment()) {
                if (onBackPressedListener != null)
                    onBackPressedListener.doBack();
                else {
                    if (doubleBackToExitPressedOnce) {
                        UtilApp.exitApplication(context);
                        return;
                    }
                    Toasts.makeToast(context, binding.container, context.getResources().getString(R.string.double_backpres));
                    this.doubleBackToExitPressedOnce = true;
                    new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 3500);
                }

            } else {
                if (onBackPressedListener != null)
                    onBackPressedListener.doBack();
                else {
                    fragNavController.popFragment();
                }
            }


    }

    @Override
    public int getNumberOfRootFragments () {
        return 1;
    }

    @NotNull
    @Override
    public Fragment getRootFragment (int i) {
        switch (i) {
            case FragNavController.TAB1:
                return new HomeFragment();
            default:
                throw new IllegalStateException("unsupported tab index: " + i);
        }
    }

    @Override
    public void pushFragment (Fragment fragment) {
        this.fragNavController.pushFragment(fragment);
    }
}
