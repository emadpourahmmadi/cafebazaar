package ir.emad.pourahmadi.cafebazaar.data.model.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ir.emad.pourahmadi.cafebazaar.data.model.base.BaseResponse;


public class VenuesModel extends BaseResponse {

    @Expose
    @SerializedName("response")
    private VenuesGroup response;

    public VenuesGroup getResponse () {
        return response;
    }

    public void setResponse (VenuesGroup response) {
        this.response = response;
    }

}
