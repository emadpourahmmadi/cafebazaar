package ir.emad.pourahmadi.cafebazaar.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import ir.emad.pourahmadi.cafebazaar.R;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.Venues;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesItems;
import ir.emad.pourahmadi.cafebazaar.databinding.ItemLocationBinding;

public class LocationListAdapter extends PagedListAdapter <VenuesItems, LocationListAdapter.ContactViewHolder> {
    String TAG = LocationListAdapter.class.getSimpleName();
    private static final int VIEW_SELECT = 0;


    private listener listener;

    public LocationListAdapter () {
        super(REPO_COMPARATOR);
    }

    private static final DiffUtil.ItemCallback <VenuesItems> REPO_COMPARATOR =
            new DiffUtil.ItemCallback <VenuesItems>() {
                @Override
                public boolean areItemsTheSame (@NonNull VenuesItems oldItem, @NonNull VenuesItems newItem) {
                    return oldItem.getReferralId().equals(newItem.getReferralId());
                }

                @Override
                public boolean areContentsTheSame (@NonNull VenuesItems oldItem, @NonNull VenuesItems newItem) {
                    return oldItem.getReferralId().equals(newItem.getReferralId());
                }
            };


    public void setListener (listener listener) {
        this.listener = listener;
    }


    public interface listener {
        void onItemClickListener (int pos, VenuesItems model);
    }


    public int getItemViewType (int position) {
        return VIEW_SELECT;
    }


    @SuppressLint({"SetTextI18n", "ClickableViewAccessibility", "UseCompatLoadingForDrawables", "DefaultLocale"})
    @Override
    public void onBindViewHolder (@NonNull final ContactViewHolder holder, final int position) {
        final VenuesItems list = getItem(position);
        if (list != null) {
            final Venues model = list.getVenue();
            ItemLocationBinding bindingSelected = holder.bindingSelected;
            if (model != null) {
                bindingSelected.txtTitle.setText(model.getName() != null ? model.getName() : " - ");
                bindingSelected.txtAddress.setText(model.getLocation() != null && model.getLocation().getAddress() != null && !model.getLocation().getAddress().equals("") ? model.getLocation().getAddress() : " - ");
                bindingSelected.txtCategory.setText(model.getCategories() != null && model.getCategories().size() > 0 && model.getCategories().get(0) != null ? model.getCategories().get(0).getName() : " - ");
                bindingSelected.layoutParent.setOnClickListener(v -> listener.onItemClickListener(position, list));
            }
        }
    }


    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder (@NonNull ViewGroup viewGroup, int viewType) {
        ViewDataBinding binding;

        switch (viewType) {
            case VIEW_SELECT:
                binding = DataBindingUtil.inflate(LayoutInflater.
                        from(viewGroup.getContext()), R.layout.item_location, viewGroup, false);
                return new ContactViewHolder((ItemLocationBinding) binding, listener);
        }
        return null;


    }


    public class ContactViewHolder extends RecyclerView.ViewHolder {

        private ItemLocationBinding bindingSelected;

        private final listener listener;

        ContactViewHolder (ItemLocationBinding binding, listener listener) {
            super(binding.getRoot());
            this.bindingSelected = binding;
            this.listener = listener;
        }
    }


}
