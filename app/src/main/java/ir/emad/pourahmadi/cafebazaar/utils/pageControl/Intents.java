package ir.emad.pourahmadi.cafebazaar.utils.pageControl;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import ir.emad.pourahmadi.cafebazaar.R;
import ir.emad.pourahmadi.cafebazaar.utils.Constant;
import ir.emad.pourahmadi.cafebazaar.utils.UtilApp;

public class Intents {

    public static void startActivity (Context context, final Class <? extends Activity> ActivityToOpen, boolean finish, boolean hasAanimation,int animationType) {
        Intent intent = new Intent(context, ActivityToOpen);
        context.startActivity(intent);
        if (context instanceof AppCompatActivity) {
            if (hasAanimation){
                switch (animationType){
                    case 1:
                        ((AppCompatActivity) context).overridePendingTransition(R.anim.slide_in_right, android.R.anim.fade_out);
                        break;
                    case 2:
                        ((AppCompatActivity) context).overridePendingTransition(R.anim.slide_up, android.R.anim.fade_out);
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + animationType);
                }
            }

        }

        if (finish) {
            UtilApp.ExitAnimation(context);
        }

    }

    public static void startActivityBundle (Context context, final Class <? extends Activity> ActivityToOpen, boolean finish, int Id, String keyString) {
        Intent intent = new Intent(context, ActivityToOpen);
        intent.putExtra(Constant.ARG_INT, Id);
        if (keyString != null && !keyString.equals("")) {
            intent.putExtra(Constant.ARG_STRING, keyString);
        }
        context.startActivity(intent);

        if (finish) {
            UtilApp.ExitAnimation(context);
        }
    }

    public static void startActivityBundleOtp (Context context, final Class <? extends Activity> ActivityToOpen
            , boolean finish, boolean isNewUser, String mobile) {
        Intent intent = new Intent(context, ActivityToOpen);
        intent.putExtra(Constant.ARG_Boolean, isNewUser);
        intent.putExtra(Constant.ARG_STRING, mobile);
        context.startActivity(intent);
        if (finish) {
            UtilApp.ExitAnimation(context);
        }
    }

    public static void startActivityWebview (Context context, final Class <? extends Activity> ActivityToOpen,
                                             boolean finish, String titlePage, String URL) {
        Intent intent = new Intent(context, ActivityToOpen);
        intent.putExtra("titlePage", titlePage);
        intent.putExtra("url", URL);
        context.startActivity(intent);
        if (context instanceof AppCompatActivity) {
            ((AppCompatActivity) context).overridePendingTransition(R.anim.slide_in_right, android.R.anim.fade_out);
        }

        if (finish) {
            UtilApp.ExitAnimation(context);
        }
    }

/*
    public static void startFullscreenIActivity (Activity context, SliderLayout imageView, List <String> serviceModel,
                                                 String imageCover) {
        Intent intent = new Intent(context, FullscreenImageActivity.class);
        int location[] = new int[2];
        imageView.getLocationOnScreen(location);

        Bundle bundle = new Bundle();
        bundle.putParcelable("model", Parcels.wrap(serviceModel));
        bundle.putString("image", imageCover);
        bundle.putInt("left", location[0]);
        bundle.putInt("top", location[1]);
        bundle.putInt("height", imageView.getHeight());
        bundle.putInt("width", imageView.getWidth());


        intent.putExtras(bundle);

        context.startActivity(intent);
        context.overridePendingTransition(0, 0);
    }
*/

}
