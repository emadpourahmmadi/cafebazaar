package ir.emad.pourahmadi.cafebazaar.ui.features.fragments.home;


public interface HomeNavigator {

    void menuClick ();

    void responseLocationSetting (Exception ex);

    void setTotalOffset (int totalResults);
}