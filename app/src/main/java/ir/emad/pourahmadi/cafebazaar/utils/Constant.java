package ir.emad.pourahmadi.cafebazaar.utils;

/**
 * Created by Abhinav Singh on 09,June,2020
 */
public class Constant {
    public static final String Android = "Android";

    public static final String ARG_STRING = "String";
    public static final String ARG_STRING2 = "String2";
    public static final String ARG_STRING3 = "String3";
    public static final String ARG_MODEL = "list";
    public static final String ARG_MODEL2 = "list2";
    public static final String ARG_INT = "int";
    public static final String ARG_Boolean = "boolean";
    public static final int EmptyList = 0;
    public static final int ShowView = 1;
    public static final int Cash = 48;
    public static final int Online = 49;
    public static final int Wallet = 50;
    public static final int Credit = 113;
    public static String notifTypePage = "page";
    public static String notifTypeRequestDetails = "request_details";
    public static String orderIdKey = "order_id";
    public static String userIdKey = "user_id";
    public static String conversationIdKey = "conversation_id";

    public static long retryServer = 3;
    public static int successResponse = 200;
    public static int AppResultCode = 200;
    public static int AppFirebaseResultCode = 9002;
    public static int mobileLength = 11;
    public static int mobileLengthZero = 10;
    public static int smsLength = 5;
    public static String DataBaseName = "Emad";
    public static int userId = 1;
    public static int manGenderId = 6;
    public static int womanGenderId = 7;
    public static int jobsConfig = 1;
    public static int colorConfig = 3;
    public static int typeLeft = 1;
    public static int typeUp = 2;
    public static int animationTime = 8000;
    public static int animationTimeLine = 1000;
    public static int taskLenght = 3;
    public static String urlConfig = "General/api/GetDefineCategory?category=";
    public static int fromProfile = 1;
    public static String taskWork = "database";
    public static String firstSync = "first";
    public static String syncStatus = "Status";
    public static String mPreferncesName = "mPreferncesName";
    public static String clientId = "5B4WFSGIUR03Y4D1H23L0CGQWFHMNSQO53ECQQYAOISGPRDG";
    public static String clientSecret = "BCV403KCEBNYMFL0BBRSOQWICSOT3FVZFEOUFZTONT32TPGT";
    public static String firstRunPrefKey = "FirstTimeRun";
    public static String venuDetailUrl = "venues/";
    public static String imageUrl = "https://igx.4sqi.net/img/general/600x800";
    public static float locationChange = 1000;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 2 * 50000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    public static int pageLimit = 10;
    public static int defaultOfffset = 10;
}


