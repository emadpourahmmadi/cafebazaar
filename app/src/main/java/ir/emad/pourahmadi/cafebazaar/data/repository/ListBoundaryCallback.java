package ir.emad.pourahmadi.cafebazaar.data.repository;


import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import io.reactivex.rxjava3.schedulers.Schedulers;
import ir.emad.pourahmadi.cafebazaar.data.model.base.baseBoundaryCallback.BaseBoundaryCallback;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesItems;
import ir.emad.pourahmadi.cafebazaar.data.network.ApiInterface;
import ir.emad.pourahmadi.cafebazaar.db.database.UserDao;
import ir.emad.pourahmadi.cafebazaar.db.database.VenuDao;
import ir.emad.pourahmadi.cafebazaar.utils.Constant;

public class ListBoundaryCallback extends BaseBoundaryCallback {

    ApiInterface apiService;
    VenuDao venuDao;
    UserDao userDao;
    int mPage;

    @Inject
    public ListBoundaryCallback (VenuDao local, UserDao userDao, ApiInterface remote) {
        super(local, userDao, remote);
    }

    /**
     * Called when zero items are returned from an initial load of the PagedList's data source.
     */
    @Override
    public void onZeroItemsLoaded () {
        mPage = 1;
        loadAndSave();
    }

    /**
     * Called when the item at the end of the PagedList has been loaded, and access has occurred
     * within Config.prefetchDistance of it. No more data will be appended to the PagedList after
     * this item.
     *
     * @param itemAtEnd The first item of PagedList
     */
    @Override
    public void onItemAtEndLoaded (@NonNull VenuesItems itemAtEnd) {
        super.onItemAtEndLoaded(itemAtEnd);

        nextPage();
        loadAndSave();
    }

    private void refresh () {
        userDao.getData().observe((LifecycleOwner) this, userData -> {
            if (userData != null && userData.getLastLocation() != null) {
                loadAndSave();
            }
        });

    }
    private void loadAndSave () {

        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
        String formattedDate = df.format(Calendar.getInstance().getTime());
            apiService.explore((userDao.getUserData().getLastLocation().latitude + "," + userDao.getUserData().getLastLocation().longitude), Constant.clientId,
                    Constant.clientSecret, formattedDate, Constant.pageLimit, mPage * Constant.defaultOfffset)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe(data -> {
                        mPage = data.getResponse().getTotalResults();
                        venuDao.insert(data.getResponse().getGroups().get(0).getItems());
                    }, throwable -> {
                        Log.e("ZERO_ITEM_POPULAR_ERROR", throwable.getMessage());
                    });
    }
}