package ir.emad.pourahmadi.cafebazaar.data.model.dataModel;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import ir.emad.pourahmadi.cafebazaar.db.database.DateConverter;

@Parcel(Parcel.Serialization.BEAN)
@Entity(tableName = "UserLogin")
public class UserLogin {
    @NonNull
    @PrimaryKey()
    @Expose
    @ColumnInfo(name = "id")
    private int id;

    @Expose
    @TypeConverters(DateConverter.class)
    @ColumnInfo(name = "lastLocation")
    private LatLng lastLocation;

    @ParcelConstructor
    public UserLogin (int id,LatLng lastLocation) {
        this.id =id;
        this.lastLocation = lastLocation;
    }

    @NonNull
    public int getId () {
        return id;
    }

    public void setId (@NonNull int id) {
        this.id = id;
    }

    public LatLng getLastLocation () {
        return lastLocation;
    }

    public void setLastLocation (LatLng lastLocation) {
        this.lastLocation = lastLocation;
    }
}
