package ir.emad.pourahmadi.cafebazaar.ui.customViews.sheets;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import ir.emad.pourahmadi.cafebazaar.R;

public abstract class BaseSheet<B extends ViewDataBinding, T extends ViewModel> extends BottomSheetDialogFragment {

    protected boolean isKeyboard = false;
    protected T viewModel;
    protected B binding;

    @Override
    public int getTheme () {
        return R.style.BottomSheetDialogFragmentTheme;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog (Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(dialogs -> {
            BottomSheetDialog d = (BottomSheetDialog) dialogs;
            FrameLayout bottomSheet = d.findViewById(R.id.design_bottom_sheet);
            assert bottomSheet != null;
            BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
        });
        if (dialog.getWindow() != null)
            if (this.isKeyboard)
                dialog.getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE |
                                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            else dialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE |
                            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        return dialog;
    }

    @Override
    public void onCreate (@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        beforeView();
    }

    @Nullable
    @Override
    public View onCreateView (@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        ViewCompat.setLayoutDirection(binding.getRoot(), ViewCompat.LAYOUT_DIRECTION_LTR);
        binding.setLifecycleOwner(this);
        viewModel = new ViewModelProvider(this).get(getViewModel());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated (@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        afterView();
    }


    @Override
    public void onDestroyView () {
        super.onDestroyView();
        destroyView();
    }

    public abstract
    @LayoutRes
    int getLayoutId ();

    protected abstract Class <T> getViewModel ();

    protected abstract B getViewBinding ();

    public abstract void beforeView ();

    public abstract void afterView () ;

    public abstract void destroyView ();
}
