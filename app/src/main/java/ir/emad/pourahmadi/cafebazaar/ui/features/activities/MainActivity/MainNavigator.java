package ir.emad.pourahmadi.cafebazaar.ui.features.activities.MainActivity;


import java.util.UUID;

public interface MainNavigator {

   void bundleSyncId (UUID uuid);

}