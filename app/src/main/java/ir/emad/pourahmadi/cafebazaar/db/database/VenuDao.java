package ir.emad.pourahmadi.cafebazaar.db.database;

import java.util.List;

import androidx.paging.DataSource;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesItems;



@Dao
public interface VenuDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert (List <VenuesItems> model);

    @Query("DELETE FROM VenuesItems")
    void clearData ();

    @Query("SELECT * FROM VenuesItems")
    DataSource.Factory<Integer, VenuesItems> getData();

}