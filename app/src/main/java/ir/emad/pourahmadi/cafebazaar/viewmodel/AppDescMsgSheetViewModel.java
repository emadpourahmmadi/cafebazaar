package ir.emad.pourahmadi.cafebazaar.viewmodel;


import ir.emad.pourahmadi.cafebazaar.ui.customViews.sheets.appDesc.AppDescMsgSheetNavigator;
import ir.emad.pourahmadi.cafebazaar.ui.features.activities.base.BaseViewModel;

public class AppDescMsgSheetViewModel extends BaseViewModel <AppDescMsgSheetNavigator> {
    private static final String TAG = AppDescMsgSheetViewModel.class.getSimpleName();

    public void onCancelClick () {
        getNavigator().CancelClick();
    }


}
