package ir.emad.pourahmadi.cafebazaar.ui.features.fragments.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

import com.bumptech.glide.RequestManager;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.maps.model.LatLng;
import com.tbruyelle.rxpermissions3.RxPermissions;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dagger.hilt.android.AndroidEntryPoint;
import ir.emad.pourahmadi.cafebazaar.BuildConfig;
import ir.emad.pourahmadi.cafebazaar.R;
import ir.emad.pourahmadi.cafebazaar.adapters.LocationListAdapter;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesItems;
import ir.emad.pourahmadi.cafebazaar.databinding.FragmentHomeBinding;
import ir.emad.pourahmadi.cafebazaar.db.database.AppDatabase;
import ir.emad.pourahmadi.cafebazaar.ui.customViews.sheets.appDesc.AppDescMsgSheet;
import ir.emad.pourahmadi.cafebazaar.ui.features.activities.MainActivity.MainActivity;
import ir.emad.pourahmadi.cafebazaar.ui.features.fragments.base.BaseFragment;
import ir.emad.pourahmadi.cafebazaar.ui.features.fragments.detail.DetailFragment;
import ir.emad.pourahmadi.cafebazaar.utils.Constant;
import ir.emad.pourahmadi.cafebazaar.utils.CustomToast.Toasts;
import ir.emad.pourahmadi.cafebazaar.utils.UtilApp;
import ir.emad.pourahmadi.cafebazaar.viewmodel.HomeViewModel;

import static android.app.Activity.RESULT_OK;

@AndroidEntryPoint
public class HomeFragment extends BaseFragment <FragmentHomeBinding, HomeViewModel> implements HomeNavigator, MainActivity.OnBackPressedListener, LocationListAdapter.listener {

    private static final int REQUEST_CODE_CHECK_SETTINGS = 1;
    private String TAG = HomeFragment.class.getSimpleName();
    @Inject
    RequestManager glide;
    @Inject
    AppDatabase database;

    private FragmentActivity context;
    private RxPermissions rxPermissions;
    private LocationListAdapter adapter;
    private LatLng firstLocation;
    private LatLng secLocation;
    private int offset = Constant.defaultOfffset;
    private boolean isLoading = false;
    private int totalOffset;
    private LinearLayoutManager linearLayoutManager;


    @Override
    public int getLayoutId () {
        return R.layout.fragment_home;
    }

    @Override
    protected Class <HomeViewModel> getViewModel () {
        return HomeViewModel.class;
    }

    @Override
    protected FragmentHomeBinding getViewBinding () {
        return binding;
    }

    @Override
    public void getContexts (Activity activity) {
        context = (FragmentActivity) activity;
    }


    @Override
    public void beforeView () {
    }

    @SuppressLint({"SetTextI18n", "CheckResult"})
    @Override
    public void afterView () {
        viewModel.setNavigator(this);
        binding.setViewModel(viewModel);
        initViews();

        viewModel.getLoading().observe(this, aBoolean -> {
            binding.layoutLoading.loading.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
        });
        viewModel.getError().observe(this, s -> {
            Toasts.makeToast(context, binding.rvList, s != null ? s + "" : context.getResources().getString(R.string.errorData));
        });


        if (viewModel.getUserLogin() != null && viewModel.getUserLogin().getLastLocation() != null) {
            firstLocation = viewModel.getUserLogin().getLastLocation();
        }

        viewModel.getUserData().observe(context, userData -> {
            if (userData != null && userData.getLastLocation() != null) {
                secLocation = userData.getLastLocation();
                if (firstLocation != null) {
                    Location locationA = new Location("point A");
                    locationA.setLatitude(firstLocation.latitude);
                    locationA.setLongitude(firstLocation.longitude);

                    Location locationB = new Location("point B");
                    locationB.setLatitude(userData.getLastLocation().latitude);
                    locationB.setLongitude(userData.getLastLocation().longitude);


                    float distance = locationA.distanceTo(locationB);
                    Log.d(TAG, "afterView: distance = " + distance);
                    if (distance >= Constant.locationChange)
                        viewModel.makeRequest(userData.getLastLocation(), offset);
                } else
                    viewModel.makeRequest(userData.getLastLocation(), offset);

            }
        });


        viewModel.getVenuData().observe(context, model -> {
            if (model != null) {
                firstLocation = secLocation;
                //  totalOffset = model.getTotalResults();
                if (model.size() > 0) {
                    SpannableStringBuilder passed = UtilApp.bgDayPassedBuilder(
                            context.getResources().getColor(R.color.bgDayTitle),
                            context.getResources().getColor(R.color.black),
                            " " + model.size() + " ", context.getResources().getString(R.string.nearByMarketCount),
                            35, 0, 0);
                    if (model.size() > 0 && model.get(0) != null)
                        totalOffset = model.get(0).getTotalResults();
                    offset = model.size();

                    binding.txtDayTitle.setText(passed);
                    adapter.submitList(model);
                    isLoading = false;
                }
            }
        });

        binding.rvList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled (@NotNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

                if (!isLoading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                        isLoading = true;
                        if (offset < totalOffset) {
                            offset = offset + Constant.defaultOfffset;
                            if (firstLocation != null)
                                viewModel.makeRequest(firstLocation, offset);

                        }


                    }
                }
            }
        });


    }

/*
    private void setListData (List <VenuesItems> items) {
        if (items != null) {
            isLoading = false;

            if (items.size() > 0) {
                list = items;
                adapter.submitList(items);
            }


        */
/*    if (offset == 1) {
                if (items.size() > 0) {
                    list.clear();
                    list = items;
                    adapter.setData(items);
                }
            } else {
                list.addAll(items);
                adapter.addItems(items);
            }*//*

        }

    }
*/

    @Override
    public void destroyView () {
        viewModel.removeLocationUpdates();
    }

    private void initViews () {

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(context, binding.drawerLayout, R.string.double_backpres, R.string.double_backpres) {
            private float scaleFactor = 5f;

            @Override
            public void onDrawerSlide (View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float slideX = drawerView.getWidth() * slideOffset;
                binding.layoutParent.setTranslationX(slideX);
                binding.layoutParent.setScaleX(1 - (slideOffset / scaleFactor));
                binding.layoutParent.setScaleY(1 - (slideOffset / scaleFactor));

                ((MainActivity) requireActivity()).setOnBackPressedListener(() -> {
                    if (binding.drawerLayout.isDrawerOpen(Gravity.LEFT))
                        binding.drawerLayout.closeDrawer(Gravity.LEFT);
                    else {
                        ((MainActivity) requireActivity()).setOnBackPressedListener(null);
                        context.onBackPressed();
                    }
                });


            }
        };
        binding.drawer.txtVersion.setText(context.getResources().getString(R.string.verison, BuildConfig.VERSION_NAME));
        binding.drawerLayout.setScrimColor(Color.TRANSPARENT);
        binding.drawerLayout.setDrawerElevation(2f);
        binding.drawerLayout.addDrawerListener(actionBarDrawerToggle);
        binding.drawer.txtCallMe.setOnClickListener(v -> {
            binding.drawerLayout.closeDrawer(Gravity.LEFT);
            UtilApp.callNumber(context, context.getResources().getString(R.string.callMeNumber));
        });
        binding.drawer.txtMenuAbout.setOnClickListener(v -> {
            binding.drawerLayout.closeDrawer(Gravity.LEFT);
            UtilApp.openBrowser(context, context.getResources().getString(R.string.aboutMeUrl));
        });
        binding.drawer.txtMenuSamples.setOnClickListener(v -> {
            binding.drawerLayout.closeDrawer(Gravity.LEFT);
            UtilApp.openTelegram(context, context.getResources().getString(R.string.telegramUrl));
        });
        binding.drawer.txtMenuSignOut.setOnClickListener(v -> {
            binding.drawerLayout.closeDrawer(Gravity.LEFT);
            UtilApp.exitSignOut(context, database);
        });
        binding.drawer.txtMenuAboutApp.setOnClickListener(v -> {
            binding.drawerLayout.closeDrawer(Gravity.LEFT);
            AppDescMsgSheet dialogFragment = new AppDescMsgSheet();
            UtilApp.showSheet(context, dialogFragment);
        });

        rxPermissions = new RxPermissions(this); // where this is an Activity or Fragment instance
        rxPermissions.setLogging(true);

        rxPermissions
                .requestEachCombined(Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe(permission -> { // will emit 1 Permission object
                    if (permission.granted) {
                        // All requested permissions are granted
                        statusCheck();
                    } else if (permission.shouldShowRequestPermissionRationale) {
                        // At least one denied permission without ask never again
                    } else {
                        // At least one permission is denied
                    }
                });
        linearLayoutManager = new LinearLayoutManager(context);
        binding.rvList.setLayoutManager(linearLayoutManager);
        adapter = new LocationListAdapter();
        binding.rvList.setAdapter(adapter);
        adapter.setListener(this);

    }

    private void pushFragment (Fragment fragment) {
        this.mFragmentNavigation.pushFragment(fragment);
    }

    @Override
    public void menuClick () {
        binding.drawerLayout.openDrawer(Gravity.LEFT);
        ((MainActivity) requireActivity()).setOnBackPressedListener(this);

    }

    @Override
    public void responseLocationSetting (Exception ex) {
        try {
            // Show the dialog by calling startResolutionForResult(),  and check the result in onActivityResult().
            startIntentSenderForResult(Objects.requireNonNull(((ResolvableApiException) ex).getStatus().getResolution()).getIntentSender(),
                    REQUEST_CODE_CHECK_SETTINGS, null, 0, 0, 0, null);
        } catch (IntentSender.SendIntentException sendEx) {
            // Ignore the error.
        }

    }

    @Override
    public void setTotalOffset (int totalResults) {
        totalOffset = totalResults;
    }

    @Override
    public void doBack () {
        if (binding.drawerLayout.isDrawerOpen(Gravity.LEFT))
            binding.drawerLayout.closeDrawer(Gravity.LEFT);
        else {
            ((MainActivity) requireActivity()).setOnBackPressedListener(null);
            context.onBackPressed();
        }

    }

    @Override
    public void onItemClickListener (int pos, VenuesItems model) {
        pushFragment(DetailFragment.newInstance(model.getVenue()));
    }

    public void statusCheck () {
        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            viewModel.requestLocationUpdates();
        }
    }

    private void buildAlertMessageNoGps () {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, id) -> viewModel.enableLocationSettings(context))
                .setNegativeButton("No", (dialog, id) -> dialog.cancel());
        final AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_CHECK_SETTINGS) {
                viewModel.requestLocationUpdates();
            }
        }
    }

}
