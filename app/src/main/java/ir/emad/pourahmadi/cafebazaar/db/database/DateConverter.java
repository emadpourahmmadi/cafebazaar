package ir.emad.pourahmadi.cafebazaar.db.database;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.room.TypeConverter;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.Venues;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesItems;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesList;

public class DateConverter {

    @NonNull
    @TypeConverter
    public String fromLatLng (@NonNull LatLng data) {
        Gson gson = new Gson();
        Type type = new TypeToken < LatLng>() {
        }.getType();
        return gson.toJson(data, type);
    }

    @NonNull
    @TypeConverter
    public LatLng toLatLng (@NonNull String data) {
        Gson gson = new Gson();
        Type type = new TypeToken < LatLng>() {
        }.getType();
        return gson.fromJson(data, type);
    }

    @NonNull
    @TypeConverter
    public String fromVenuesList(@NonNull List<VenuesList> data) {
        Gson gson = new Gson();
        Type type = new TypeToken <List<VenuesList>>() {
        }.getType();
        return gson.toJson(data, type);
    }

    @NonNull
    @TypeConverter
    public List<VenuesList> toVenuesList (@NonNull String data) {
        Gson gson = new Gson();
        Type type = new TypeToken < List<VenuesList>>() {
        }.getType();
        return gson.fromJson(data, type);
    }

    @NonNull
    @TypeConverter
    public String fromItems(@NonNull List<VenuesItems> data) {
        Gson gson = new Gson();
        Type type = new TypeToken <List<VenuesItems>>() {
        }.getType();
        return gson.toJson(data, type);
    }

    @NonNull
    @TypeConverter
    public List<VenuesItems> toItems(@NonNull String data) {
        Gson gson = new Gson();
        Type type = new TypeToken < List<VenuesItems>>() {
        }.getType();
        return gson.fromJson(data, type);
    }
    @NonNull
    @TypeConverter
    public String fromVenue(@NonNull Venues data) {
        Gson gson = new Gson();
        Type type = new TypeToken <Venues>() {
        }.getType();
        return gson.toJson(data, type);
    }

    @NonNull
    @TypeConverter
    public Venues toVenue(@NonNull String data) {
        Gson gson = new Gson();
        Type type = new TypeToken < Venues>() {
        }.getType();
        return gson.fromJson(data, type);
    }
    @NonNull
    @TypeConverter
    public String fromVenuePage(@NonNull List<Venues.VenuePage> data) {
        Gson gson = new Gson();
        Type type = new TypeToken <List<Venues.VenuePage>>() {
        }.getType();
        return gson.toJson(data, type);
    }

    @NonNull
    @TypeConverter
    public List<Venues.VenuePage> toVenuePage(@NonNull String data) {
        Gson gson = new Gson();
        Type type = new TypeToken < List<Venues.VenuePage>>() {
        }.getType();
        return gson.fromJson(data, type);
    }
    @NonNull
    @TypeConverter
    public String fromCategories(@NonNull List<Venues.Categories> data) {
        Gson gson = new Gson();
        Type type = new TypeToken <List<Venues.Categories>>() {
        }.getType();
        return gson.toJson(data, type);
    }

    @NonNull
    @TypeConverter
    public List<Venues.Categories> toCategories(@NonNull String data) {
        Gson gson = new Gson();
        Type type = new TypeToken < List<Venues.Categories>>() {
        }.getType();
        return gson.fromJson(data, type);
    }
    @NonNull
    @TypeConverter
    public String fromLocation(@NonNull List<Venues.Location> data) {
        Gson gson = new Gson();
        Type type = new TypeToken <List<Venues.Location>>() {
        }.getType();
        return gson.toJson(data, type);
    }

    @NonNull
    @TypeConverter
    public List<Venues.Location> toLocation(@NonNull String data) {
        Gson gson = new Gson();
        Type type = new TypeToken < List<Venues.Location>>() {
        }.getType();
        return gson.fromJson(data, type);
    }
}
