package ir.emad.pourahmadi.cafebazaar.data.model.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

@Parcel(Parcel.Serialization.BEAN)
public class VenuesPhotos {

    @Expose
    @SerializedName("photos")
    private Photos photos;

    public Photos getPhotos () {
        return photos;
    }

    public void setPhotos (Photos photos) {
        this.photos = photos;
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Photos {
        @Expose
        @SerializedName("dupesRemoved")
        private int dupesRemoved;
        @Expose
        @SerializedName("items")
        private List <Items> items;
        @Expose
        @SerializedName("count")
        private int count;

        public int getDupesRemoved () {
            return dupesRemoved;
        }

        public void setDupesRemoved (int dupesRemoved) {
            this.dupesRemoved = dupesRemoved;
        }

        public List <Items> getItems () {
            return items;
        }

        public void setItems (List <Items> items) {
            this.items = items;
        }

        public int getCount () {
            return count;
        }

        public void setCount (int count) {
            this.count = count;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Items {
        @Expose
        @SerializedName("visibility")
        private String visibility;
        @Expose
        @SerializedName("checkin")
        private Checkin checkin;
        @Expose
        @SerializedName("user")
        private User user;
        @Expose
        @SerializedName("height")
        private int height;
        @Expose
        @SerializedName("width")
        private int width;
        @Expose
        @SerializedName("suffix")
        private String suffix;
        @Expose
        @SerializedName("prefix")
        private String prefix;
        @Expose
        @SerializedName("source")
        private Source source;
        @Expose
        @SerializedName("createdAt")
        private int createdAt;
        @Expose
        @SerializedName("id")
        private String id;

        public String getVisibility () {
            return visibility;
        }

        public void setVisibility (String visibility) {
            this.visibility = visibility;
        }

        public Checkin getCheckin () {
            return checkin;
        }

        public void setCheckin (Checkin checkin) {
            this.checkin = checkin;
        }

        public User getUser () {
            return user;
        }

        public void setUser (User user) {
            this.user = user;
        }

        public int getHeight () {
            return height;
        }

        public void setHeight (int height) {
            this.height = height;
        }

        public int getWidth () {
            return width;
        }

        public void setWidth (int width) {
            this.width = width;
        }

        public String getSuffix () {
            return suffix;
        }

        public void setSuffix (String suffix) {
            this.suffix = suffix;
        }

        public String getPrefix () {
            return prefix;
        }

        public void setPrefix (String prefix) {
            this.prefix = prefix;
        }

        public Source getSource () {
            return source;
        }

        public void setSource (Source source) {
            this.source = source;
        }

        public int getCreatedAt () {
            return createdAt;
        }

        public void setCreatedAt (int createdAt) {
            this.createdAt = createdAt;
        }

        public String getId () {
            return id;
        }

        public void setId (String id) {
            this.id = id;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Checkin {
        @Expose
        @SerializedName("timeZoneOffset")
        private int timeZoneOffset;
        @Expose
        @SerializedName("type")
        private String type;
        @Expose
        @SerializedName("createdAt")
        private int createdAt;
        @Expose
        @SerializedName("id")
        private String id;

        public int getTimeZoneOffset () {
            return timeZoneOffset;
        }

        public void setTimeZoneOffset (int timeZoneOffset) {
            this.timeZoneOffset = timeZoneOffset;
        }

        public String getType () {
            return type;
        }

        public void setType (String type) {
            this.type = type;
        }

        public int getCreatedAt () {
            return createdAt;
        }

        public void setCreatedAt (int createdAt) {
            this.createdAt = createdAt;
        }

        public String getId () {
            return id;
        }

        public void setId (String id) {
            this.id = id;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class User {
        @Expose
        @SerializedName("countryCode")
        private String countryCode;
        @Expose
        @SerializedName("lastName")
        private String lastName;
        @Expose
        @SerializedName("firstName")
        private String firstName;
        @Expose
        @SerializedName("isSanctioned")
        private boolean isSanctioned;

        public String getCountryCode () {
            return countryCode;
        }

        public void setCountryCode (String countryCode) {
            this.countryCode = countryCode;
        }

        public String getLastName () {
            return lastName;
        }

        public void setLastName (String lastName) {
            this.lastName = lastName;
        }

        public String getFirstName () {
            return firstName;
        }

        public void setFirstName (String firstName) {
            this.firstName = firstName;
        }

        public boolean getIsSanctioned () {
            return isSanctioned;
        }

        public void setIsSanctioned (boolean isSanctioned) {
            this.isSanctioned = isSanctioned;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Source {
        @Expose
        @SerializedName("url")
        private String url;
        @Expose
        @SerializedName("name")
        private String name;

        public String getUrl () {
            return url;
        }

        public void setUrl (String url) {
            this.url = url;
        }

        public String getName () {
            return name;
        }

        public void setName (String name) {
            this.name = name;
        }
    }
}
