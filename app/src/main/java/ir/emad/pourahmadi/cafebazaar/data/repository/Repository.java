package ir.emad.pourahmadi.cafebazaar.data.repository;


import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.paging.DataSource;
import io.reactivex.rxjava3.core.Observable;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.UserLogin;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesItems;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesModel;
import ir.emad.pourahmadi.cafebazaar.data.network.ApiInterface;
import ir.emad.pourahmadi.cafebazaar.db.database.UserDao;
import ir.emad.pourahmadi.cafebazaar.db.database.VenuDao;
import ir.emad.pourahmadi.cafebazaar.utils.Constant;

public class Repository {
    private static final String TAG = "Repository";
    private final UserDao userDao;
    private final VenuDao venuDao;

    ApiInterface apiService;

    @Inject
    public Repository (ApiInterface apiService, UserDao userDao, VenuDao venuDao) {
        this.apiService = apiService;
        this.userDao = userDao;
        this.venuDao = venuDao;
    }

    public void insertUserData (UserLogin data) {
        userDao.insert(data);
    }

    public void updateLocation (LatLng data) {
        userDao.updateLocation(1, data);
    }

    public void clearUserData () {
        userDao.clearData();
    }

    public LiveData <UserLogin> getUserData () {
        return userDao.getData();
    }

    public UserLogin getUser () {
        return userDao.getUserData();
    }

    public void insertVenuList (List <VenuesItems> data) {
        venuDao.insert(data);
    }

    public DataSource.Factory <Integer, VenuesItems> getVenuData () {
        return venuDao.getData();
    }

    public Observable <VenuesModel> getVenuesList (LatLng model, String currentDate,int PageOffset) {
        return apiService.explore((model.latitude + "," + model.longitude), Constant.clientId, Constant.clientSecret, currentDate,Constant.pageLimit,PageOffset );
    }

    public Observable getVenuDetail (String venuId, String currentDate) {
        return apiService.detail(Constant.venuDetailUrl+venuId + "?client_id=" + Constant.clientId + "&client_secret=" + Constant.clientSecret + "&v=" + currentDate);
    }
    public Observable getVenuPhotos (String venuId, String currentDate) {
        return apiService.photos(Constant.venuDetailUrl+venuId +"/photos"+ "?client_id=" + Constant.clientId + "&client_secret=" + Constant.clientSecret + "&v=" + currentDate);
    }

/*
    public Observable <TaskList> synceTasks (SyncHeader model) {
        return apiService.syncTasks(model);
    }*/

}
