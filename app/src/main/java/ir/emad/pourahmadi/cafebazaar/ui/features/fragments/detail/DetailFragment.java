package ir.emad.pourahmadi.cafebazaar.ui.features.fragments.detail;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;

import com.bumptech.glide.RequestManager;
import com.glide.slider.library.SliderTypes.DefaultSliderView;

import org.parceler.Parcels;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import dagger.hilt.android.AndroidEntryPoint;
import ir.emad.pourahmadi.cafebazaar.BuildConfig;
import ir.emad.pourahmadi.cafebazaar.R;
import ir.emad.pourahmadi.cafebazaar.adapters.AttributesListAdapter;
import ir.emad.pourahmadi.cafebazaar.adapters.CategoryListAdapter;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.Venues;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesDetail;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesPhotos;
import ir.emad.pourahmadi.cafebazaar.databinding.FragmentDetailBinding;
import ir.emad.pourahmadi.cafebazaar.db.database.AppDatabase;
import ir.emad.pourahmadi.cafebazaar.ui.customViews.sheets.appDesc.AppDescMsgSheet;
import ir.emad.pourahmadi.cafebazaar.ui.features.activities.MainActivity.MainActivity;
import ir.emad.pourahmadi.cafebazaar.ui.features.fragments.base.BaseFragment;
import ir.emad.pourahmadi.cafebazaar.utils.Constant;
import ir.emad.pourahmadi.cafebazaar.utils.CustomToast.Toasts;
import ir.emad.pourahmadi.cafebazaar.utils.FileUtil;
import ir.emad.pourahmadi.cafebazaar.utils.UtilApp;
import ir.emad.pourahmadi.cafebazaar.viewmodel.DetailViewModel;

@AndroidEntryPoint
public class DetailFragment extends BaseFragment <FragmentDetailBinding, DetailViewModel> implements DetailNavigator, MainActivity.OnBackPressedListener {

    private String TAG = DetailFragment.class.getSimpleName();
    @Inject
    RequestManager glide;
    @Inject
    AppDatabase database;
    private AttributesListAdapter adapter;
    private CategoryListAdapter categoryListAdapter;

    private FragmentActivity context;
    private Venues model;
    private VenuesDetail.Venue detailModel;

    @NonNull
    public static DetailFragment newInstance (@NonNull Venues model) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(Constant.ARG_MODEL, Parcels.wrap(model));
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public int getLayoutId () {
        return R.layout.fragment_detail;
    }

    @Override
    protected Class <DetailViewModel> getViewModel () {
        return DetailViewModel.class;
    }

    @Override
    protected FragmentDetailBinding getViewBinding () {
        return binding;
    }

    @Override
    public void getContexts (Activity activity) {
        context = (FragmentActivity) activity;
    }


    @Override
    public void beforeView () {
        if (getArguments() != null) {
            model = Parcels.unwrap(getArguments().getParcelable(Constant.ARG_MODEL));
        }
    }

    @SuppressLint({"SetTextI18n", "CheckResult"})
    @Override
    public void afterView () {
        viewModel.setNavigator(this);
        binding.setViewModel(viewModel);

        initViews();

        viewModel.getLoading().observe(this, aBoolean -> {
            binding.layoutLoading.loading.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
        });
        viewModel.getError().observe(this, s -> {
            Toasts.makeToast(context, binding.imgMenu, s != null ? s + "" : context.getResources().getString(R.string.errorData));
        });


        viewModel.getUserData().observe(context, userData -> {
        });

        viewModel.makeRequest(model.getId());
        viewModel.makePhotosRequest(model.getId());

    }

    @Override
    public void destroyView () {
    }

    private void initViews () {

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(context, binding.drawerLayout, R.string.double_backpres, R.string.double_backpres) {
            private float scaleFactor = 5f;

            @Override
            public void onDrawerSlide (View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float slideX = drawerView.getWidth() * slideOffset;
                binding.layoutParent.setTranslationX(slideX);
                binding.layoutParent.setScaleX(1 - (slideOffset / scaleFactor));
                binding.layoutParent.setScaleY(1 - (slideOffset / scaleFactor));

                ((MainActivity) requireActivity()).setOnBackPressedListener(() -> {
                    if (binding.drawerLayout.isDrawerOpen(Gravity.LEFT))
                        binding.drawerLayout.closeDrawer(Gravity.LEFT);
                    else {
                        ((MainActivity) requireActivity()).setOnBackPressedListener(null);
                        context.onBackPressed();
                    }
                });


            }
        };
        binding.drawer.txtVersion.setText(context.getResources().getString(R.string.verison, BuildConfig.VERSION_NAME));
        binding.drawerLayout.setScrimColor(Color.TRANSPARENT);
        binding.drawerLayout.setDrawerElevation(2f);
        binding.drawerLayout.addDrawerListener(actionBarDrawerToggle);
        binding.drawer.txtCallMe.setOnClickListener(v -> {
            binding.drawerLayout.closeDrawer(Gravity.LEFT);
            UtilApp.callNumber(context, context.getResources().getString(R.string.callMeNumber));
        });
        binding.drawer.txtMenuAbout.setOnClickListener(v -> {
            binding.drawerLayout.closeDrawer(Gravity.LEFT);
            UtilApp.openBrowser(context, context.getResources().getString(R.string.aboutMeUrl));
        });
        binding.drawer.txtMenuSamples.setOnClickListener(v -> {
            binding.drawerLayout.closeDrawer(Gravity.LEFT);
            UtilApp.openTelegram(context, context.getResources().getString(R.string.telegramUrl));
        });
        binding.drawer.txtMenuSignOut.setOnClickListener(v -> {
            binding.drawerLayout.closeDrawer(Gravity.LEFT);
            UtilApp.exitSignOut(context, database);
        });
        binding.drawer.txtMenuAboutApp.setOnClickListener(v -> {
            binding.drawerLayout.closeDrawer(Gravity.LEFT);
            AppDescMsgSheet dialogFragment = new AppDescMsgSheet();
            UtilApp.showSheet(context, dialogFragment);
        });

        binding.txtDayTitle.setText(model.getName());
        categoryListAdapter = new CategoryListAdapter();
        binding.rvCategoryList.setAdapter(categoryListAdapter);

        adapter = new AttributesListAdapter();
        binding.rvList.setAdapter(adapter);
    }

    private void pushFragment (Fragment fragment) {
        this.mFragmentNavigation.pushFragment(fragment);
    }

    @Override
    public void menuClick () {
        binding.drawerLayout.openDrawer(Gravity.LEFT);
        ((MainActivity) requireActivity()).setOnBackPressedListener(this);

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void response (VenuesDetail.Venue venue) {
        detailModel = venue;
        binding.txtRate.setText(detailModel.getRating() + "");

        if (detailModel.getAttributes() != null && detailModel.getAttributes().getGroups() != null && detailModel.getAttributes().getGroups().size() > 0) {
            adapter.setData(detailModel.getAttributes().getGroups());
            binding.rvList.setVisibility(View.VISIBLE);
            binding.txtTitle.setVisibility(View.VISIBLE);
        } else {
            binding.rvList.setVisibility(View.GONE);
            binding.txtTitle.setVisibility(View.GONE);
        }
        if (detailModel.getCategories() != null && detailModel.getCategories() != null && detailModel.getCategories().size() > 0) {
            categoryListAdapter.setData(detailModel.getCategories());
            binding.rvCategoryList.setVisibility(View.VISIBLE);
            binding.txtCategoryTitle.setVisibility(View.VISIBLE);
        } else {
            binding.rvCategoryList.setVisibility(View.GONE);
            binding.txtCategoryTitle.setVisibility(View.GONE);
        }
    }

    @Override
    public void backClick () {
        doBack();
    }

    @Override
    public void onContactClick () {
        if (detailModel != null && detailModel.getContact() != null) {
            UtilApp.callNumber(context, detailModel.getContact().getPhone());
        }
    }

    @Override
    public void onShareClick () {
        if (detailModel != null) {
            String shareMsg = " share Location  : " +
                    (detailModel.getName() != null && !detailModel.getName().equals("") ?
                            detailModel.getName() + "" : " - ") + "\n" +
                    (detailModel.getShortUrl() != null && !detailModel.getShortUrl().equals("") ?
                            detailModel.getShortUrl() + "" : " - ");

            FileUtil.shareText(context, shareMsg + "");

        }
    }

    @Override
    public void onDirectionClick () {
        if (detailModel != null) {
            UtilApp.launchGoogleMaps(context, detailModel.getLocation().getLat(), detailModel.getLocation().getLng(), model.getName());
        }
    }

    @Override
    public void responsePhotos (VenuesPhotos.Photos photos) {
        for (int i = 0; i < photos.getItems().size(); i++) {
            addToSlider(Constant.imageUrl + photos.getItems().get(i).getSuffix());
        }


    }


    @Override
    public void doBack () {
        if (binding.drawerLayout.isDrawerOpen(Gravity.LEFT))
            binding.drawerLayout.closeDrawer(Gravity.LEFT);
        else {
            ((MainActivity) requireActivity()).setOnBackPressedListener(null);
            context.onBackPressed();
        }

    }

    private void addToSlider (String image) {
        DefaultSliderView sliderView = new DefaultSliderView(context);
        sliderView
                .image(image)
                .setProgressBarVisible(true);
        binding.slider.addSlider(sliderView);

    }
}
