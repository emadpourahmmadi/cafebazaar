package ir.emad.pourahmadi.cafebazaar.ui.features.activities.base;

import java.lang.ref.WeakReference;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class BaseViewModel<N> extends ViewModel {

    private MutableLiveData <Boolean> loading = new MutableLiveData <>();
    private MutableLiveData <String> error = new MutableLiveData <>();

    private WeakReference <N> mNavigator = new WeakReference <>(null);


    protected void setError (String error) {
        this.error.setValue(error);
    }

    protected void setLoading (boolean isLoading) {
        this.loading.setValue(isLoading);
    }

    public MutableLiveData <Boolean> getLoading () {
        return loading;
    }

    public MutableLiveData <String> getError () {
        return error;
    }

    public N getNavigator () {
        return mNavigator.get();
    }

    public void setNavigator (N navigator) {
        this.mNavigator = new WeakReference <>(navigator);
    }
}
