package ir.emad.pourahmadi.cafebazaar.data.model.base.BaseObservable;


import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;


/**
 * Created by Chenwy on 2018/1/5 16:28
 */

public abstract class BaseObserver<T> implements Observer <T> {

    public BaseObserver () {
        showLoading();
    }

    @Override
    public abstract void onSubscribe (Disposable d);

    @Override
    public void onNext (T t) {

        hideLoading();
        onRespSuccess(t);
    }

    @Override
    public void onError (Throwable e) {
        hideLoading();
        onRespError(e);

    }

    @Override
    public void onComplete () {
    }


    protected abstract void onRespSuccess (T t);

    protected abstract void onRespError (Throwable e);

    protected abstract void showLoading ();

    protected abstract void hideLoading ();
}
