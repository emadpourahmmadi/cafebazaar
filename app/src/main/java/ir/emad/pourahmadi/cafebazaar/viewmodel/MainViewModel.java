package ir.emad.pourahmadi.cafebazaar.viewmodel;

import android.content.SharedPreferences;

import java.util.Date;

import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import ir.emad.pourahmadi.cafebazaar.App;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.UserLogin;
import ir.emad.pourahmadi.cafebazaar.data.repository.Repository;
import ir.emad.pourahmadi.cafebazaar.ui.features.activities.MainActivity.MainNavigator;
import ir.emad.pourahmadi.cafebazaar.ui.features.activities.base.BaseViewModel;
import ir.emad.pourahmadi.cafebazaar.utils.Constant;

import static android.content.Context.MODE_PRIVATE;

public class MainViewModel extends BaseViewModel <MainNavigator> {
    private static final String TAG = MainViewModel.class.getSimpleName();
    private final UserLogin userData;

    private Repository repository;

    private CompositeDisposable disposable = new CompositeDisposable();
    private SharedPreferences mPreferences;
    private static final String mPreferncesName = "MyPerfernces";
    private MutableLiveData <Date> dateTime = new MutableLiveData <>();

    @ViewModelInject
    public MainViewModel (Repository repository) {
        this.repository = repository;
        userData = repository.getUser();
        mPreferences = App.getINSTANCE().getSharedPreferences(mPreferncesName, MODE_PRIVATE);
    }

    public UserLogin getUserData () {
        return userData;
    }

    public MutableLiveData <Date> getDateTime () {
        return dateTime;
    }

    private void SaveData (String str) {
        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
        preferencesEditor.putString(Constant.syncStatus, str);
        preferencesEditor.apply();
    }


}
