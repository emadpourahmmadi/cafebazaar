package ir.emad.pourahmadi.cafebazaar.data.model.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseResponse {

    @SerializedName("meta")
    @Expose
    private Meta meta;

    public Meta getMeta () {
        return meta;
    }

    public void setMeta (Meta meta) {
        this.meta = meta;
    }

    public class Meta {
        @Expose
        @SerializedName("code")
        private int code;
        @Expose
        @SerializedName("requestId")
        private String requestId;

        public int getCode () {
            return code;
        }

        public void setCode (int code) {
            this.code = code;
        }

        public String getRequestId () {
            return requestId;
        }

        public void setRequestId (String requestId) {
            this.requestId = requestId;
        }
    }
}
