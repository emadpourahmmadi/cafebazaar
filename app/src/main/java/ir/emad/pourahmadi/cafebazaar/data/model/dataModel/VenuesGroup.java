package ir.emad.pourahmadi.cafebazaar.data.model.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

import androidx.room.TypeConverters;
import ir.emad.pourahmadi.cafebazaar.db.database.DateConverter;

@Parcel(Parcel.Serialization.BEAN)
public class VenuesGroup {
    @Expose
    @SerializedName("groups")
    @TypeConverters(DateConverter.class)
    private List <VenuesList> groups;
    @Expose
    @SerializedName("totalResults")
    private int totalResults;
    @Expose
    @SerializedName("headerLocationGranularity")
    private String headerLocationGranularity;
    @Expose
    @SerializedName("headerFullLocation")
    private String headerFullLocation;
    @Expose
    @SerializedName("headerLocation")
    private String headerLocation;
    @Expose
    @SerializedName("suggestedRadius")
    private int suggestedRadius;

    public List <VenuesList> getGroups () {
        return groups;
    }

    public void setGroups (List <VenuesList> groups) {
        this.groups = groups;
    }

    public int getTotalResults () {
        return totalResults;
    }

    public void setTotalResults (int totalResults) {
        this.totalResults = totalResults;
    }

    public String getHeaderLocationGranularity () {
        return headerLocationGranularity;
    }

    public void setHeaderLocationGranularity (String headerLocationGranularity) {
        this.headerLocationGranularity = headerLocationGranularity;
    }

    public String getHeaderFullLocation () {
        return headerFullLocation;
    }

    public void setHeaderFullLocation (String headerFullLocation) {
        this.headerFullLocation = headerFullLocation;
    }

    public String getHeaderLocation () {
        return headerLocation;
    }

    public void setHeaderLocation (String headerLocation) {
        this.headerLocation = headerLocation;
    }

    public int getSuggestedRadius () {
        return suggestedRadius;
    }

    public void setSuggestedRadius (int suggestedRadius) {
        this.suggestedRadius = suggestedRadius;
    }

}
