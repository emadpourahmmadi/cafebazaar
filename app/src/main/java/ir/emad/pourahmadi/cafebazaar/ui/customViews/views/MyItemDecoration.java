package ir.emad.pourahmadi.cafebazaar.ui.customViews.views;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class MyItemDecoration
        extends RecyclerView.ItemDecoration {

    private int extraMargin;
    private final int extraMarginDw;

    @Override
    public void getItemOffsets (Rect outRect, View view,
                                RecyclerView parent, RecyclerView.State state) {

        int position = parent.getChildAdapterPosition(view);

   /*     // It's easy to put extra margin on the last item...
        if (position + 1 == parent.getAdapter().getItemCount()) {
            outRect.left = extraMarginDw; // unit is px
        }*/

        // ...or you could give each item in the RecyclerView different
        // margins based on its position...
        if (position % 2 == 0) {
            outRect.top = extraMargin;
        } else {
            outRect.bottom = extraMarginDw;
        }

        

/*
        // ...or based on some property of the item itself
        if (position == 0) {
            outRect.left = extraMarginDw;
        }*/
    }

    public MyItemDecoration () {
        extraMargin = 50;
        extraMarginDw = 10;
    }
}