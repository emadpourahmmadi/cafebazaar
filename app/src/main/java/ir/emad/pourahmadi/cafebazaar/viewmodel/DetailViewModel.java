package ir.emad.pourahmadi.cafebazaar.viewmodel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import ir.emad.pourahmadi.cafebazaar.data.model.base.BaseObservable.BaseObserver;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.UserLogin;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesDetailModel;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesPhotosModel;
import ir.emad.pourahmadi.cafebazaar.data.repository.Repository;
import ir.emad.pourahmadi.cafebazaar.ui.features.activities.base.BaseViewModel;
import ir.emad.pourahmadi.cafebazaar.ui.features.fragments.detail.DetailNavigator;
import ir.emad.pourahmadi.cafebazaar.utils.Constant;

public class DetailViewModel extends BaseViewModel <DetailNavigator> {
    private static final String TAG = DetailViewModel.class.getSimpleName();
    private final LiveData <UserLogin> userData;
    private final UserLogin userLogin;

    private Repository repository;

    private CompositeDisposable disposable = new CompositeDisposable();


    @ViewModelInject
    public DetailViewModel (Repository repository) {
        this.repository = repository;
        userData = repository.getUserData();
        userLogin = repository.getUser();
    }

    public UserLogin getUserLogin () {
        return userLogin;
    }

    public LiveData <UserLogin> getUserData () {
        return userData;
    }

    public void onMenuClick () {
        getNavigator().menuClick();
    }

    public void onBackClick () {
        getNavigator().backClick();
    }

    public void onContactClick () {
        getNavigator().onContactClick();
    }

    public void onShareClick () {
        getNavigator().onShareClick();
    }

    public void onDirectionClick () {
        getNavigator().onDirectionClick();
    }

    public void makeRequest (String venuId) {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
        String formattedDate = df.format(Calendar.getInstance().getTime());
        repository.getVenuDetail(venuId, formattedDate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(Constant.retryServer)
                .subscribe(new BaseObserver <VenuesDetailModel>() {
                    @Override
                    public void onSubscribe (Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    protected void onRespSuccess (VenuesDetailModel model) {
                        if (model != null && model.getMeta() != null) {
                            if (model.getMeta().getCode() == Constant.successResponse) {
                                if (model.getResponse() != null && model.getResponse() != null && model.getResponse().getVenue() != null)
                                    getNavigator().response(model.getResponse().getVenue());
                            }
                        }
                    }

                    @Override
                    protected void onRespError (Throwable e) {
                        setError(null);
                    }

                    @Override
                    protected void showLoading () {
                        setLoading(true);
                    }

                    @Override
                    protected void hideLoading () {
                        setLoading(false);
                    }
                });


    }
    public void makePhotosRequest (String venuId) {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
        String formattedDate = df.format(Calendar.getInstance().getTime());
        repository.getVenuPhotos(venuId, formattedDate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(Constant.retryServer)
                .subscribe(new BaseObserver <VenuesPhotosModel>() {
                    @Override
                    public void onSubscribe (Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    protected void onRespSuccess (VenuesPhotosModel model) {
                        if (model != null && model.getMeta() != null) {
                            if (model.getMeta().getCode() == Constant.successResponse) {
                                if (model.getResponse() != null && model.getResponse() != null && model.getResponse().getPhotos() != null)
                                    getNavigator().responsePhotos(model.getResponse().getPhotos());
                            }
                        }
                    }

                    @Override
                    protected void onRespError (Throwable e) {
                        setError(null);
                    }

                    @Override
                    protected void showLoading () {
                        setLoading(true);
                    }

                    @Override
                    protected void hideLoading () {
                        setLoading(false);
                    }
                });


    }

}
