package ir.emad.pourahmadi.cafebazaar.data.model.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import ir.emad.pourahmadi.cafebazaar.db.database.DateConverter;

@Parcel(Parcel.Serialization.BEAN)
@Entity(tableName = "VenuesItems")
public class VenuesItems {
    @NonNull
    @PrimaryKey()
    @Expose
    @ColumnInfo(name = "referralId")
    @SerializedName("referralId")
    private String referralId;
    @Expose
    @ColumnInfo(name = "venue")
    @TypeConverters(DateConverter.class)
    @SerializedName("venue")
    private Venues venue;
    @Expose
    @ColumnInfo(name = "totalResults")
    @SerializedName("totalResults")
    private int totalResults;

    public int getTotalResults () {
        return totalResults;
    }

    public void setTotalResults (int totalResults) {
        this.totalResults = totalResults;
    }

    public Venues getVenue () {
        return venue;
    }

    public void setVenue (Venues venue) {
        this.venue = venue;
    }

    @NonNull
    public String getReferralId () {
        return referralId;
    }

    public void setReferralId (@NonNull String referralId) {
        this.referralId = referralId;
    }
}
