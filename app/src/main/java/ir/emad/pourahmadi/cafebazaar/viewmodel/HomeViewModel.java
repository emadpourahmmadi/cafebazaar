package ir.emad.pourahmadi.cafebazaar.viewmodel;

import android.app.Activity;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import ir.emad.pourahmadi.cafebazaar.App;
import ir.emad.pourahmadi.cafebazaar.data.model.base.BaseObservable.BaseObserver;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.UserLogin;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesItems;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesModel;
import ir.emad.pourahmadi.cafebazaar.data.repository.Repository;
import ir.emad.pourahmadi.cafebazaar.ui.features.activities.base.BaseViewModel;
import ir.emad.pourahmadi.cafebazaar.ui.features.fragments.home.HomeNavigator;
import ir.emad.pourahmadi.cafebazaar.utils.Constant;

import static android.content.Context.MODE_PRIVATE;

public class HomeViewModel extends BaseViewModel <HomeNavigator> {
    private static final String TAG = HomeViewModel.class.getSimpleName();
    private final LiveData <UserLogin> userData;
    private final UserLogin userLogin;
    public final LiveData <PagedList <VenuesItems>> venuData;

    private Repository repository;

    private CompositeDisposable disposable = new CompositeDisposable();

    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private Location location;

    @ViewModelInject
    public HomeViewModel (Repository repository) {
        this.repository = repository;
        userData = repository.getUserData();
        userLogin = repository.getUser();

        venuData = new LivePagedListBuilder <>(
                repository.getVenuData(), 10).build();

        getLocation();
    }

    public LiveData <PagedList <VenuesItems>> getVenuData () {
        return venuData;
    }

    public UserLogin getUserLogin () {
        return userLogin;
    }

    public LiveData <UserLogin> getUserData () {
        return userData;
    }

    public void makeRequest (LatLng location, int pageOffset) {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
        String formattedDate = df.format(Calendar.getInstance().getTime());
        repository.getVenuesList(location, formattedDate, pageOffset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(Constant.retryServer)
                .subscribe(new BaseObserver <VenuesModel>() {
                    @Override
                    public void onSubscribe (Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    protected void onRespSuccess (VenuesModel model) {
                        if (model != null && model.getMeta() != null) {
                            if (model.getMeta().getCode() == Constant.successResponse) {
                                getNavigator().setTotalOffset(model.getResponse().getTotalResults());
                                if (model.getResponse().getGroups() != null && model.getResponse().getGroups().size() > 0){
                                    model.getResponse().getGroups().get(0).getItems().get(0).setTotalResults(model.getResponse().getTotalResults());
                                    repository.insertVenuList(model.getResponse().getGroups().get(0).getItems());
                                }
                            }
                        }
                    }

                    @Override
                    protected void onRespError (Throwable e) {
                        setError(null);
                    }

                    @Override
                    protected void showLoading () {
                        setLoading(true);
                    }

                    @Override
                    protected void hideLoading () {
                        setLoading(false);
                    }
                });


    }

    public void onMenuClick () {
        getNavigator().menuClick();
    }

    public void firstUserLoginData () {
        final SharedPreferences shared1 = App.getINSTANCE().getSharedPreferences("FirstTimeRun", MODE_PRIVATE);
        SharedPreferences.Editor editor = shared1.edit();
        boolean isFirstStart = shared1.getBoolean(Constant.firstRunPrefKey, true);
        if (isFirstStart && location != null) {
            repository.insertUserData(new UserLogin(1, new LatLng(location.getLatitude(), location.getLongitude())));
            editor.putBoolean(Constant.firstRunPrefKey, false);
            editor.apply();
        }
    }


    public void getLocation () {

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(App.getINSTANCE());

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult (LocationResult locationResult) {
                super.onLocationResult(locationResult);
                onNewLocation(locationResult.getLastLocation());
                firstUserLoginData();
            }
        };
        createLocationRequest();
        getLastLocation();

    }

    public void enableLocationSettings (Activity context) {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        LocationServices
                .getSettingsClient(context)
                .checkLocationSettings(builder.build())
                .addOnSuccessListener(context, (LocationSettingsResponse response) -> {
                    requestLocationUpdates();
                })
                .addOnFailureListener(context, ex -> {
                    if (ex instanceof ResolvableApiException) {
                        getNavigator().responseLocationSetting(ex);
                    }
                });
    }

    private void createLocationRequest () {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(Constant.UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(Constant.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public void requestLocationUpdates () {
        Log.i(TAG, "Requesting location updates");
        try {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback, Looper.myLooper());
        } catch (SecurityException unlikely) {
            Log.d(TAG, "Lost location permission. Could not request updates. " + unlikely);
        }
    }

    public void removeLocationUpdates () {
        Log.i(TAG, "Removing location updates");
        try {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        } catch (SecurityException unlikely) {
            Log.d(TAG, "Lost location permission. Could not remove updates. " + unlikely);
        }
    }

    private void getLastLocation () {
        try {
            mFusedLocationClient.getLastLocation()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful() && task.getResult() != null) {
                            location = task.getResult();
                            onNewLocation(location);
                            Log.d(TAG, " getLastLocation: " + task.getResult());

                        } else {
                            Log.w(TAG, "Failed to get location.");
                        }
                    });
        } catch (SecurityException unlikely) {
            Log.d(TAG, "Lost location permission." + unlikely);
        }
    }

    private void onNewLocation (Location task) {
        Log.d(TAG, "New location: " + task);
        location = task;
        repository.updateLocation(new LatLng(task.getLatitude(), task.getLongitude()));
    }

}
