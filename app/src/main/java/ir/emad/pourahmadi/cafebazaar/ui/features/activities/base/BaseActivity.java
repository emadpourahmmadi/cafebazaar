package ir.emad.pourahmadi.cafebazaar.ui.features.activities.base;


import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public abstract class BaseActivity<B extends ViewDataBinding, T extends ViewModel> extends AppCompatActivity {

    String TAG = BaseActivity.class.getSimpleName();

    protected T viewModel;
    protected B binding;

    @Override
    protected void onCreate (@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            binding = DataBindingUtil.setContentView(this, getLayout());
            binding.setLifecycleOwner(this);
            viewModel = new ViewModelProvider(this).get(getViewModel());

            beforeView();
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            afterView(savedInstanceState);


    }

    @Override
    protected void onDestroy () {
        super.onDestroy();
        destroyView();
    }

    @Override
    protected void attachBaseContext (Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    public abstract void beforeView ();

    protected abstract @LayoutRes
    int getLayout ();

    protected abstract Class <T> getViewModel ();

    protected abstract B getViewBinding ();

    public abstract void afterView (Bundle savedInstanceState);


    public abstract void destroyView ();

}