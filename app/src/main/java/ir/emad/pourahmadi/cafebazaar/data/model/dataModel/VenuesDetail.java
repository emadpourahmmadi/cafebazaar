package ir.emad.pourahmadi.cafebazaar.data.model.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

@Parcel(Parcel.Serialization.BEAN)
public class VenuesDetail {

    @Expose
    @SerializedName("venue")
    private Venue venue;

    public Venue getVenue () {
        return venue;
    }

    public void setVenue (Venue venue) {
        this.venue = venue;
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Venue {
        @Expose
        @SerializedName("colors")
        private Colors colors;
        @Expose
        @SerializedName("bestPhoto")
        private BestPhoto bestPhoto;
        @Expose
        @SerializedName("attributes")
        private Attributes attributes;
        @Expose
        @SerializedName("timeZone")
        private String timeZone;
        @Expose
        @SerializedName("shortUrl")
        private String shortUrl;
        @Expose
        @SerializedName("tips")
        private Tips tips;
        @Expose
        @SerializedName("createdAt")
        private int createdAt;
        @Expose
        @SerializedName("photos")
        private Photos photos;
        @Expose
        @SerializedName("allowMenuUrlEdit")
        private boolean allowMenuUrlEdit;
        @Expose
        @SerializedName("ratingSignals")
        private int ratingSignals;
        @Expose
        @SerializedName("ratingColor")
        private String ratingColor;
        @Expose
        @SerializedName("rating")
        private double rating;
        @Expose
        @SerializedName("ok")
        private boolean ok;
        @Expose
        @SerializedName("dislike")
        private boolean dislike;
        @Expose
        @SerializedName("likes")
        private Likes likes;
        @Expose
        @SerializedName("stats")
        private Stats stats;
        @Expose
        @SerializedName("verified")
        private boolean verified;
        @Expose
        @SerializedName("categories")
        private List <Categories> categories;
        @Expose
        @SerializedName("canonicalUrl")
        private String canonicalUrl;
        @Expose
        @SerializedName("location")
        private Location location;
        @Expose
        @SerializedName("contact")
        private Contact contact;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private String id;

        public Colors getColors () {
            return colors;
        }

        public void setColors (Colors colors) {
            this.colors = colors;
        }

        public BestPhoto getBestPhoto () {
            return bestPhoto;
        }

        public void setBestPhoto (BestPhoto bestPhoto) {
            this.bestPhoto = bestPhoto;
        }

        public Attributes getAttributes () {
            return attributes;
        }

        public void setAttributes (Attributes attributes) {
            this.attributes = attributes;
        }


        public String getTimeZone () {
            return timeZone;
        }

        public void setTimeZone (String timeZone) {
            this.timeZone = timeZone;
        }

        public String getShortUrl () {
            return shortUrl;
        }

        public void setShortUrl (String shortUrl) {
            this.shortUrl = shortUrl;
        }

        public Tips getTips () {
            return tips;
        }

        public void setTips (Tips tips) {
            this.tips = tips;
        }

        public int getCreatedAt () {
            return createdAt;
        }

        public void setCreatedAt (int createdAt) {
            this.createdAt = createdAt;
        }


        public Photos getPhotos () {
            return photos;
        }

        public void setPhotos (Photos photos) {
            this.photos = photos;
        }


        public boolean getAllowMenuUrlEdit () {
            return allowMenuUrlEdit;
        }

        public void setAllowMenuUrlEdit (boolean allowMenuUrlEdit) {
            this.allowMenuUrlEdit = allowMenuUrlEdit;
        }

        public int getRatingSignals () {
            return ratingSignals;
        }

        public void setRatingSignals (int ratingSignals) {
            this.ratingSignals = ratingSignals;
        }

        public String getRatingColor () {
            return ratingColor;
        }

        public void setRatingColor (String ratingColor) {
            this.ratingColor = ratingColor;
        }

        public double getRating () {
            return rating;
        }

        public void setRating (double rating) {
            this.rating = rating;
        }

        public boolean getOk () {
            return ok;
        }

        public void setOk (boolean ok) {
            this.ok = ok;
        }

        public boolean getDislike () {
            return dislike;
        }

        public void setDislike (boolean dislike) {
            this.dislike = dislike;
        }

        public Likes getLikes () {
            return likes;
        }

        public void setLikes (Likes likes) {
            this.likes = likes;
        }

        public Stats getStats () {
            return stats;
        }

        public void setStats (Stats stats) {
            this.stats = stats;
        }

        public boolean getVerified () {
            return verified;
        }

        public void setVerified (boolean verified) {
            this.verified = verified;
        }

        public List <Categories> getCategories () {
            return categories;
        }

        public void setCategories (List <Categories> categories) {
            this.categories = categories;
        }

        public String getCanonicalUrl () {
            return canonicalUrl;
        }

        public void setCanonicalUrl (String canonicalUrl) {
            this.canonicalUrl = canonicalUrl;
        }

        public Location getLocation () {
            return location;
        }

        public void setLocation (Location location) {
            this.location = location;
        }

        public Contact getContact () {
            return contact;
        }

        public void setContact (Contact contact) {
            this.contact = contact;
        }

        public String getName () {
            return name;
        }

        public void setName (String name) {
            this.name = name;
        }

        public String getId () {
            return id;
        }

        public void setId (String id) {
            this.id = id;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Colors {
        @Expose
        @SerializedName("algoVersion")
        private int algoVersion;
        @Expose
        @SerializedName("highlightTextColor")
        private HighlightTextColor highlightTextColor;
        @Expose
        @SerializedName("highlightColor")
        private HighlightColor highlightColor;

        public int getAlgoVersion () {
            return algoVersion;
        }

        public void setAlgoVersion (int algoVersion) {
            this.algoVersion = algoVersion;
        }

        public HighlightTextColor getHighlightTextColor () {
            return highlightTextColor;
        }

        public void setHighlightTextColor (HighlightTextColor highlightTextColor) {
            this.highlightTextColor = highlightTextColor;
        }

        public HighlightColor getHighlightColor () {
            return highlightColor;
        }

        public void setHighlightColor (HighlightColor highlightColor) {
            this.highlightColor = highlightColor;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class HighlightTextColor {
        @Expose
        @SerializedName("value")
        private int value;
        @Expose
        @SerializedName("photoId")
        private String photoId;

        public int getValue () {
            return value;
        }

        public void setValue (int value) {
            this.value = value;
        }

        public String getPhotoId () {
            return photoId;
        }

        public void setPhotoId (String photoId) {
            this.photoId = photoId;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class HighlightColor {
        @Expose
        @SerializedName("value")
        private int value;
        @Expose
        @SerializedName("photoId")
        private String photoId;

        public int getValue () {
            return value;
        }

        public void setValue (int value) {
            this.value = value;
        }

        public String getPhotoId () {
            return photoId;
        }

        public void setPhotoId (String photoId) {
            this.photoId = photoId;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class BestPhoto {
        @Expose
        @SerializedName("visibility")
        private String visibility;
        @Expose
        @SerializedName("height")
        private int height;
        @Expose
        @SerializedName("width")
        private int width;
        @Expose
        @SerializedName("suffix")
        private String suffix;
        @Expose
        @SerializedName("prefix")
        private String prefix;
        @Expose
        @SerializedName("source")
        private Source source;
        @Expose
        @SerializedName("createdAt")
        private int createdAt;
        @Expose
        @SerializedName("id")
        private String id;

        public String getVisibility () {
            return visibility;
        }

        public void setVisibility (String visibility) {
            this.visibility = visibility;
        }

        public int getHeight () {
            return height;
        }

        public void setHeight (int height) {
            this.height = height;
        }

        public int getWidth () {
            return width;
        }

        public void setWidth (int width) {
            this.width = width;
        }

        public String getSuffix () {
            return suffix;
        }

        public void setSuffix (String suffix) {
            this.suffix = suffix;
        }

        public String getPrefix () {
            return prefix;
        }

        public void setPrefix (String prefix) {
            this.prefix = prefix;
        }

        public Source getSource () {
            return source;
        }

        public void setSource (Source source) {
            this.source = source;
        }

        public int getCreatedAt () {
            return createdAt;
        }

        public void setCreatedAt (int createdAt) {
            this.createdAt = createdAt;
        }

        public String getId () {
            return id;
        }

        public void setId (String id) {
            this.id = id;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Source {
        @Expose
        @SerializedName("url")
        private String url;
        @Expose
        @SerializedName("name")
        private String name;

        public String getUrl () {
            return url;
        }

        public void setUrl (String url) {
            this.url = url;
        }

        public String getName () {
            return name;
        }

        public void setName (String name) {
            this.name = name;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Attributes {
        @Expose
        @SerializedName("groups")
        private List <Groups> groups;

        public List <Groups> getGroups () {
            return groups;
        }

        public void setGroups (List <Groups> groups) {
            this.groups = groups;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Groups {
        @Expose
        @SerializedName("items")
        private List <Items> items;
        @Expose
        @SerializedName("count")
        private int count;
        @Expose
        @SerializedName("summary")
        private String summary;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("type")
        private String type;

        public List <Items> getItems () {
            return items;
        }

        public void setItems (List <Items> items) {
            this.items = items;
        }

        public int getCount () {
            return count;
        }

        public void setCount (int count) {
            this.count = count;
        }

        public String getSummary () {
            return summary;
        }

        public void setSummary (String summary) {
            this.summary = summary;
        }

        public String getName () {
            return name;
        }

        public void setName (String name) {
            this.name = name;
        }

        public String getType () {
            return type;
        }

        public void setType (String type) {
            this.type = type;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Items {
        @Expose
        @SerializedName("suffix")
        private String suffix;
        @Expose
        @SerializedName("displayName")
        private String displayName;

        public String getSuffix () {
            return suffix;
        }

        public void setSuffix (String suffix) {
            this.suffix = suffix;
        }

        public String getDisplayName () {
            return displayName;
        }

        public void setDisplayName (String displayName) {
            this.displayName = displayName;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class ListItems {
        @Expose
        @SerializedName("items")
        private List <Items> items;
        @Expose
        @SerializedName("count")
        private int count;

        public List <Items> getItems () {
            return items;
        }

        public void setItems (List <Items> items) {
            this.items = items;
        }

        public int getCount () {
            return count;
        }

        public void setCount (int count) {
            this.count = count;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Followers {
        @Expose
        @SerializedName("count")
        private int count;

        public int getCount () {
            return count;
        }

        public void setCount (int count) {
            this.count = count;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Photo {
        @Expose
        @SerializedName("visibility")
        private String visibility;
        @Expose
        @SerializedName("user")
        private User user;
        @Expose
        @SerializedName("height")
        private int height;
        @Expose
        @SerializedName("width")
        private int width;
        @Expose
        @SerializedName("suffix")
        private String suffix;
        @Expose
        @SerializedName("prefix")
        private String prefix;
        @Expose
        @SerializedName("createdAt")
        private int createdAt;
        @Expose
        @SerializedName("id")
        private String id;

        public String getVisibility () {
            return visibility;
        }

        public void setVisibility (String visibility) {
            this.visibility = visibility;
        }

        public User getUser () {
            return user;
        }

        public void setUser (User user) {
            this.user = user;
        }

        public int getHeight () {
            return height;
        }

        public void setHeight (int height) {
            this.height = height;
        }

        public int getWidth () {
            return width;
        }

        public void setWidth (int width) {
            this.width = width;
        }

        public String getSuffix () {
            return suffix;
        }

        public void setSuffix (String suffix) {
            this.suffix = suffix;
        }

        public String getPrefix () {
            return prefix;
        }

        public void setPrefix (String prefix) {
            this.prefix = prefix;
        }

        public int getCreatedAt () {
            return createdAt;
        }

        public void setCreatedAt (int createdAt) {
            this.createdAt = createdAt;
        }

        public String getId () {
            return id;
        }

        public void setId (String id) {
            this.id = id;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class User {
        @Expose
        @SerializedName("countryCode")
        private String countryCode;
        @Expose
        @SerializedName("lastName")
        private String lastName;
        @Expose
        @SerializedName("firstName")
        private String firstName;
        @Expose
        @SerializedName("isSanctioned")
        private boolean isSanctioned;

        public String getCountryCode () {
            return countryCode;
        }

        public void setCountryCode (String countryCode) {
            this.countryCode = countryCode;
        }

        public String getLastName () {
            return lastName;
        }

        public void setLastName (String lastName) {
            this.lastName = lastName;
        }

        public String getFirstName () {
            return firstName;
        }

        public void setFirstName (String firstName) {
            this.firstName = firstName;
        }

        public boolean getIsSanctioned () {
            return isSanctioned;
        }

        public void setIsSanctioned (boolean isSanctioned) {
            this.isSanctioned = isSanctioned;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Tips {
        @Expose
        @SerializedName("groups")
        private List <Groups> groups;
        @Expose
        @SerializedName("count")
        private int count;

        public List <Groups> getGroups () {
            return groups;
        }

        public void setGroups (List <Groups> groups) {
            this.groups = groups;
        }

        public int getCount () {
            return count;
        }

        public void setCount (int count) {
            this.count = count;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Todo {
        @Expose
        @SerializedName("count")
        private int count;

        public int getCount () {
            return count;
        }

        public void setCount (int count) {
            this.count = count;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Likes {
        @Expose
        @SerializedName("summary")
        private String summary;
        @Expose
        @SerializedName("groups")
        private List <Groups> groups;
        @Expose
        @SerializedName("count")
        private int count;

        public String getSummary () {
            return summary;
        }

        public void setSummary (String summary) {
            this.summary = summary;
        }

        public List <Groups> getGroups () {
            return groups;
        }

        public void setGroups (List <Groups> groups) {
            this.groups = groups;
        }

        public int getCount () {
            return count;
        }

        public void setCount (int count) {
            this.count = count;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Photos {
        @Expose
        @SerializedName("groups")
        private List <Groups> groups;
        @Expose
        @SerializedName("count")
        private int count;

        public List <Groups> getGroups () {
            return groups;
        }

        public void setGroups (List <Groups> groups) {
            this.groups = groups;
        }

        public int getCount () {
            return count;
        }

        public void setCount (int count) {
            this.count = count;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Stats {
        @Expose
        @SerializedName("tipCount")
        private int tipCount;

        public int getTipCount () {
            return tipCount;
        }

        public void setTipCount (int tipCount) {
            this.tipCount = tipCount;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Categories {
        @Expose
        @SerializedName("primary")
        private boolean primary;
        @Expose
        @SerializedName("icon")
        private Icon icon;
        @Expose
        @SerializedName("shortName")
        private String shortName;
        @Expose
        @SerializedName("pluralName")
        private String pluralName;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private String id;

        public boolean getPrimary () {
            return primary;
        }

        public void setPrimary (boolean primary) {
            this.primary = primary;
        }

        public Icon getIcon () {
            return icon;
        }

        public void setIcon (Icon icon) {
            this.icon = icon;
        }

        public String getShortName () {
            return shortName;
        }

        public void setShortName (String shortName) {
            this.shortName = shortName;
        }

        public String getPluralName () {
            return pluralName;
        }

        public void setPluralName (String pluralName) {
            this.pluralName = pluralName;
        }

        public String getName () {
            return name;
        }

        public void setName (String name) {
            this.name = name;
        }

        public String getId () {
            return id;
        }

        public void setId (String id) {
            this.id = id;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Icon {
        @Expose
        @SerializedName("suffix")
        private String suffix;
        @Expose
        @SerializedName("prefix")
        private String prefix;

        public String getSuffix () {
            return suffix;
        }

        public void setSuffix (String suffix) {
            this.suffix = suffix;
        }

        public String getPrefix () {
            return prefix;
        }

        public void setPrefix (String prefix) {
            this.prefix = prefix;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Location {
        @Expose
        @SerializedName("formattedAddress")
        private List <String> formattedAddress;
        @Expose
        @SerializedName("country")
        private String country;
        @Expose
        @SerializedName("state")
        private String state;
        @Expose
        @SerializedName("city")
        private String city;
        @Expose
        @SerializedName("neighborhood")
        private String neighborhood;
        @Expose
        @SerializedName("cc")
        private String cc;
        @Expose
        @SerializedName("labeledLatLngs")
        private List <LabeledLatLngs> labeledLatLngs;
        @Expose
        @SerializedName("lng")
        private double lng;
        @Expose
        @SerializedName("lat")
        private double lat;
        @Expose
        @SerializedName("address")
        private String address;

        public List <String> getFormattedAddress () {
            return formattedAddress;
        }

        public void setFormattedAddress (List <String> formattedAddress) {
            this.formattedAddress = formattedAddress;
        }

        public String getCountry () {
            return country;
        }

        public void setCountry (String country) {
            this.country = country;
        }

        public String getState () {
            return state;
        }

        public void setState (String state) {
            this.state = state;
        }

        public String getCity () {
            return city;
        }

        public void setCity (String city) {
            this.city = city;
        }

        public String getNeighborhood () {
            return neighborhood;
        }

        public void setNeighborhood (String neighborhood) {
            this.neighborhood = neighborhood;
        }

        public String getCc () {
            return cc;
        }

        public void setCc (String cc) {
            this.cc = cc;
        }

        public List <LabeledLatLngs> getLabeledLatLngs () {
            return labeledLatLngs;
        }

        public void setLabeledLatLngs (List <LabeledLatLngs> labeledLatLngs) {
            this.labeledLatLngs = labeledLatLngs;
        }

        public double getLng () {
            return lng;
        }

        public void setLng (double lng) {
            this.lng = lng;
        }

        public double getLat () {
            return lat;
        }

        public void setLat (double lat) {
            this.lat = lat;
        }

        public String getAddress () {
            return address;
        }

        public void setAddress (String address) {
            this.address = address;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class LabeledLatLngs {
        @Expose
        @SerializedName("lng")
        private double lng;
        @Expose
        @SerializedName("lat")
        private double lat;
        @Expose
        @SerializedName("label")
        private String label;

        public double getLng () {
            return lng;
        }

        public void setLng (double lng) {
            this.lng = lng;
        }

        public double getLat () {
            return lat;
        }

        public void setLat (double lat) {
            this.lat = lat;
        }

        public String getLabel () {
            return label;
        }

        public void setLabel (String label) {
            this.label = label;
        }
    }

    @Parcel(Parcel.Serialization.BEAN)
    public static class Contact {
        @Expose
        @SerializedName("formattedPhone")
        private String formattedPhone;
        @Expose
        @SerializedName("phone")
        private String phone;

        public String getFormattedPhone () {
            return formattedPhone;
        }

        public void setFormattedPhone (String formattedPhone) {
            this.formattedPhone = formattedPhone;
        }

        public String getPhone () {
            return phone;
        }

        public void setPhone (String phone) {
            this.phone = phone;
        }
    }
}
