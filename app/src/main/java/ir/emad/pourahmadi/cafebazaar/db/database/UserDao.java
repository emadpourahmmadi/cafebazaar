package ir.emad.pourahmadi.cafebazaar.db.database;

import com.google.android.gms.maps.model.LatLng;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.UserLogin;

/**
 * Created by Abhinav Singh on 14,June,2020
 */

@Dao
public interface UserDao {
/*

    @Query("INSERT OR REPLACE INTO UserLogin (favoritColor) VALUES (:color)")
    void insertRegister (String color );

    @Query("UPDATE UserLogin SET favoritColor=:color WHERE id = :id")
    void setColor (int id, String color);
    @Query("UPDATE UserLogin SET gender=:gender WHERE id = :id")
    void setGender (int id, int gender);
    @Query("UPDATE UserLogin SET firstName=:name WHERE id = :id")
    void setName (int id, String name);
    @Query("UPDATE UserLogin SET job=:job WHERE id = :id")
    void setJob (int id, String job);
    @Query("UPDATE UserLogin SET birthDate=:birthDate WHERE id = :id")
    void setBirthday (int id, String birthDate);
*/



    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert (UserLogin model);

    @Query("UPDATE UserLogin SET lastLocation=:model WHERE id = :id ")
    void updateLocation (int id, LatLng model);

    @Query("DELETE FROM UserLogin")
    void clearData ();

    @Query("SELECT * FROM UserLogin")
    LiveData <UserLogin> getData ();

    @Query("SELECT * FROM UserLogin")
    UserLogin getUserData ();
}
