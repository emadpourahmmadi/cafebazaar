package ir.emad.pourahmadi.cafebazaar.ui.features.activities.checkInternet;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import dagger.hilt.android.AndroidEntryPoint;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import ir.emad.pourahmadi.cafebazaar.R;
import ir.emad.pourahmadi.cafebazaar.databinding.ActivityCheckInternetBinding;

@AndroidEntryPoint
public class CheckInternetActivity extends AppCompatActivity {

    ActivityCheckInternetBinding binding;


    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_check_internet);
        binding.btnRetry.setOnClickListener(v -> {
            if (isOnline()) {
                finish();
            }
        });

    }

    public boolean isOnline () {

        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;
        if (cm != null) {
            netInfo = cm.getActiveNetworkInfo();
        }
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());
    }

    @Override
    protected void attachBaseContext (Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

}
