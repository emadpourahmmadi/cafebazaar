package ir.emad.pourahmadi.cafebazaar.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.Random;

import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;
import ir.emad.pourahmadi.cafebazaar.BuildConfig;
import ir.emad.pourahmadi.cafebazaar.R;

import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;
import static android.content.Intent.FLAG_GRANT_WRITE_URI_PERMISSION;

/**
 * Created by Emad on 27/02/2018.
 */

public class FileUtil {

    private static int checkFile;
    private static Intent intent;

    public static Uri getImageContentUri (Context context, File imageFile) {
        String filePath = imageFile.getAbsolutePath();
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[] {MediaStore.Images.Media._ID},
                MediaStore.Images.Media.DATA + "=? ",
                new String[] {filePath}, null);
        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            cursor.close();
            return Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + id);
        } else {
            if (imageFile.exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, filePath);
                return context.getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return null;
            }
        }
    }

    public static void CheckAndRotateImage (Bitmap bitmap, String photoPath) {
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(photoPath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            Bitmap rotatedBitmap = null;
            switch (orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(bitmap, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(bitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(bitmap, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = bitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static String getMimeType (String path) {

        String extension = MimeTypeMap.getFileExtensionFromUrl(path);

        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
    }

    private static Bitmap rotateImage (Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public static String readableFileSize (long size) {
        if (size <= 0) return "0";
        final String[] units = new String[] {"B", "kB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    public static String randomString () {

        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();

        System.out.println(generatedString);
        return generatedString;
    }

    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    public static boolean copyToClipboard (Context context, String text) {
        try {
            int sdk = Build.VERSION.SDK_INT;
            if (sdk < Build.VERSION_CODES.HONEYCOMB) {
                android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context
                        .getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setText(text);
            } else {
                ClipboardManager clipboard = (ClipboardManager) context
                        .getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData
                        .newPlainText(
                                context.getResources().getString(
                                        R.string.message), text);
                clipboard.setPrimaryClip(clip);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @SuppressLint("NewApi")
    public static String readFromClipboard (Context context) {
        int sdk = Build.VERSION.SDK_INT;
        if (sdk < Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context
                    .getSystemService(Context.CLIPBOARD_SERVICE);
            return clipboard.getText().toString();
        } else {
            ClipboardManager clipboard = (ClipboardManager) context
                    .getSystemService(Context.CLIPBOARD_SERVICE);

            // Gets a content resolver instance
            ContentResolver cr = context.getContentResolver();

            // Gets the clipboard data from the clipboard
            ClipData clip = clipboard.getPrimaryClip();
            if (clip != null) {

                String text = null;
                String title = null;

                // Gets the first item from the clipboard data
                ClipData.Item item = clip.getItemAt(0);

                // Tries to get the item's contents as a URI pointing to a note
                Uri uri = item.getUri();

                // If the contents of the clipboard wasn't a reference to a
                // note, then
                // this converts whatever it is to text.
                if (text == null) {
                    text = coerceToText(context, item).toString();
                }

                return text;
            }
        }
        return "";
    }

    @SuppressLint("NewApi")
    private static CharSequence coerceToText (Context context, ClipData.Item item) {
        // If this Item has an explicit textual value, simply return that.
        CharSequence text = item.getText();
        if (text != null) {
            return text;
        }

        // If this Item has a URI value, try using that.
        Uri uri = item.getUri();
        if (uri != null) {

            // First see if the URI can be opened as a plain text stream
            // (of any sub-type). If so, this is the best textual
            // representation for it.
            FileInputStream stream = null;
            try {
                // Ask for a stream of the desired type.
                AssetFileDescriptor descr = context.getContentResolver()
                        .openTypedAssetFileDescriptor(uri, "text/*", null);
                stream = descr.createInputStream();
                InputStreamReader reader = new InputStreamReader(stream,
                        "UTF-8");

                // Got it... copy the stream into a local string and return it.
                StringBuilder builder = new StringBuilder(128);
                char[] buffer = new char[8192];
                int len;
                while ((len = reader.read(buffer)) > 0) {
                    builder.append(buffer, 0, len);
                }
                return builder.toString();

            } catch (FileNotFoundException e) {
                // Unable to open content URI as text... not really an
                // error, just something to ignore.

            } catch (IOException e) {
                // Something bad has happened.
                Log.w("ClippedData", "Failure loading text", e);
                return e.toString();

            } finally {
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException e) {
                    }
                }
            }

            // If we couldn't open the URI as a stream, then the URI itself
            // probably serves fairly well as a textual representation.
            return uri.toString();
        }

        // Finally, if all we have is an Intent, then we can just turn that
        // into text. Not the most user-friendly thing, but it's something.
        Intent intent = item.getIntent();
        if (intent != null) {
            return intent.toUri(Intent.URI_INTENT_SCHEME);
        }

        // Shouldn't get here, but just in case...
        return "";
    }

/*
    public static void shareImageUrl(final Context context, final String url, final CustomDlgSimple Loading) {
        Loading.onCreateDialog();
        Picasso.with(context).load(url).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Loading.destroy();
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("image*/
/*");
                i.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(context, bitmap));
                context.startActivity(Intent.createChooser(i, context.getResources().getString(R.string.ShareTo)));


            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                Loading.destroy();
                //  Util.ShareText(context,"",url);
            }

            @Override
            public void onPrepareLoad(Drawable noimageDrawable) {
                if (noimageDrawable == null) {
                    Loading.destroy();
                    Util.ShareText(context, "", url);
                }
            }
        });
    }
*/

    public static void saveImageToExternal (Context context, String url) {
        String PATH = context.getResources().getString(R.string.app_name);
        File file = new File(PATH);
        if (!file.exists())
            file.mkdir();

        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";

//Create Path to save Image
        DownloadManager.Request request =
                new DownloadManager.Request(Uri.parse(url));
        request.setDescription(fname);
        request.setTitle(fname);
        // in order for this if to run, you must use the android 3.2 to compile your app
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        request.setDestinationInExternalPublicDir(PATH, fname);


        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

/*
    public static void SaveImageFromUrl(final Context context, String url, final AVLoadingIndicatorView Loading) {
        Picasso.with(context).load(url).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap finalBitmap, Picasso.LoadedFrom from) {

                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/saved_images");
                myDir.mkdirs();
                Random generator = new Random();
                int n = 10000;
                n = generator.nextInt(n);
                String fname = "Image-" + n + ".jpg";
                File file = new File(myDir, fname);
                if (file.exists())
                    file.delete();
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.flush();
                    out.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }

                MediaScannerConnection.scanFile(context, new String[]{file.toString()}, null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            public void onScanCompleted(String path, Uri uri) {
                                Loading.hide();

                                Log.i("ExternalStorage", "Scanned " + path + ":");
                                Log.i("ExternalStorage", "-> uri=" + uri);
                            }
                        });

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                Loading.hide();
            }

            @Override
            public void onPrepareLoad(Drawable noimageDrawable) {
                Loading.show();
            }
        });

    }
*/

    private static Uri getLocalBitmapUri (Context context, Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "اشتراک گذاری_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public static void shareImage_File (Activity context, String path) {
        final File file = new File(path);
// let the FileProvider generate an URI for this private file
        Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file);
// create an intent, so the user can choose which application he/she wants to use to share this file
        final Intent intent = ShareCompat.IntentBuilder.from(context)
                .setType("*/*")
                .setSubject(context.getString(R.string.app_name))
                .setStream(uri)
                .setChooserTitle(R.string.ShareTo)
                .createChooserIntent()
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
                .addFlags(FLAG_GRANT_READ_URI_PERMISSION);

        context.startActivity(intent);
    }

    public static String getFileFromStream (FileOutputStream fileOutputStream) {
        Field pathField = null;
        try {
            pathField = FileOutputStream.class.getDeclaredField("path");
            pathField.setAccessible(true);
            return (String) pathField.get(fileOutputStream);

        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;

    }

    public static void openFile (final Context context, final File file) {

        try {

            Uri uri = Uri.fromFile(file);

            intent = new Intent(Intent.ACTION_VIEW);
            if (file.isFile()) {
                MediaScannerConnection.scanFile(context, new String[]
                                {file.toString()}, null,
                        (path, uri1) -> {
/*                                uri= Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                                    android.support.v4.content.FileProvider.getUriForFile
                                            (context,context.getPackageName() + ".provider", file) : Uri.fromFile(file);*/

                            if (!file.getPath().contains(context.getResources().getString(R.string.app_name))) {
                                uri1 = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file);
                                intent = new Intent(Intent.ACTION_VIEW, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            }

                            if (file.toString().contains(".doc") || file.toString().contains(".docx")) {
                                // Word document
                                intent.setDataAndType(uri1, "application/msword");
                            } else if (file.toString().contains(".pdf")) {
                                // PDF file
                                intent.setDataAndType(uri1, "application/pdf");
                            } else if (file.toString().contains(".ppt") || file.toString().contains(".pptx")) {
                                // Powerpoint file
                                intent.setDataAndType(uri1, "application/vnd.ms-powerpoint");
                            } else if (file.toString().contains(".xls") || file.toString().contains(".xlsx")) {
                                // Excel file
                                intent.setDataAndType(uri1, "application/vnd.ms-excel");
                            } else if (file.toString().contains(".zip") || file.toString().contains(".rar")) {
                                // WAV audio file
                                intent.setDataAndType(uri1, "application/x-wav");
                            } else if (file.toString().contains(".rtf")) {
                                // RTF file
                                intent.setDataAndType(uri1, "application/rtf");
                            } else if (file.toString().contains(".wav") || file.toString().contains(".mp3")) {
                                // WAV audio file
                                intent.setDataAndType(uri1, "audio/x-wav");
                            } else if (file.toString().contains(".gif")) {
                                // GIF file
                                intent.setDataAndType(uri1, "image/gif");
                            } else if (file.toString().contains(".jpg") || file.toString().contains(".jpeg") || file.toString().contains(".png")) {
                                // JPG file
                                intent.setDataAndType(uri1, "image/jpeg");
                            } else if (file.toString().contains(".txt")) {
                                // Text file
                                intent.setDataAndType(uri1, "text/plain");
                            } else if (file.toString().contains(".3gp") || file.toString().contains(".mpg") ||
                                    file.toString().contains(".mpeg") || file.toString().contains(".mpe") ||
                                    file.toString().contains(".mp4") || file.toString().contains(".avi")) {
                                // Video files
                                intent.setDataAndType(uri1, "video/*");
                            } else {
                                intent.setDataAndType(uri1, "*/*");
                            }

                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | FLAG_GRANT_READ_URI_PERMISSION | FLAG_GRANT_WRITE_URI_PERMISSION);
                            context.startActivity(intent);
                        });

            }


        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "برنامه ای برای باز کردن این فایل یافت نشد.", Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean checkImageResource (Context ctx, ImageView imageView,
                                              int imageResource) {
        boolean result = false;

        if (ctx != null && imageView != null && imageView.getDrawable() != null) {
            Drawable.ConstantState constantState;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                constantState = ctx.getResources()
                        .getDrawable(imageResource, ctx.getTheme())
                        .getConstantState();
            } else {
                constantState = ctx.getResources().getDrawable(imageResource)
                        .getConstantState();
            }

            if (imageView.getDrawable().getConstantState() == constantState) {
                result = true;
            }
        }

        return result;
    }


    public static void shareText (Context context, String shareBody) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, context.getResources().getString(R.string.app_name));
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        context.startActivity(Intent.createChooser(sharingIntent, "Share To... "));
    }
}
