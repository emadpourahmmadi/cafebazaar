package ir.emad.pourahmadi.cafebazaar.data.network;

import io.reactivex.rxjava3.core.Observable;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesDetailModel;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesModel;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesPhotosModel;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiInterface {


    @GET("venues/explore/")
    Observable <VenuesModel> explore (@Query(value = "ll", encoded = true) String latlng,
                                      @Query(value = "client_id", encoded = true) String client_id,
                                      @Query(value = "client_secret", encoded = true) String client_secret,
                                      @Query(value = "v", encoded = true) String date,
                                      @Query(value = "limit", encoded = true) int limit,
                                      @Query(value = "offset", encoded = true) int pageOffset
                                      );

    @GET()
    Observable <VenuesDetailModel> detail (@Url String s);

    @GET()
    Observable <VenuesPhotosModel> photos (@Url String s);


   /* @DELETE("Task/api/DeleteTask")
    Observable <BaseResponse> deleteTask (@Query("Id") int Id);

    @PUT("Task/api/UpdateTask")
    Observable <BaseResponse> updateTask (@Body TaskList model);


    @POST("User/api/UpdateUser")
    Observable <UserLoginData> updateUser (@Body UserUpdateHeader model);
*/
}