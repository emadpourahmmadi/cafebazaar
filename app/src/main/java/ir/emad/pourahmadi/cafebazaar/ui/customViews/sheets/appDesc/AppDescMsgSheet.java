package ir.emad.pourahmadi.cafebazaar.ui.customViews.sheets.appDesc;

import android.annotation.SuppressLint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dagger.hilt.android.AndroidEntryPoint;
import ir.emad.pourahmadi.cafebazaar.R;
import ir.emad.pourahmadi.cafebazaar.adapters.AppDesListAdapter;
import ir.emad.pourahmadi.cafebazaar.databinding.SheetAppDescBinding;
import ir.emad.pourahmadi.cafebazaar.ui.customViews.sheets.BaseSheet;
import ir.emad.pourahmadi.cafebazaar.viewmodel.AppDescMsgSheetViewModel;

@AndroidEntryPoint
public class AppDescMsgSheet extends BaseSheet <SheetAppDescBinding, AppDescMsgSheetViewModel>
        implements AppDescMsgSheetNavigator {

    private List <String> model = new ArrayList <>();


    @Override
    public void CancelClick () {
        dismiss();
    }


    @Override
    public int getLayoutId () {
        return R.layout.sheet_app_desc;
    }

    @Override
    protected Class <AppDescMsgSheetViewModel> getViewModel () {
        return AppDescMsgSheetViewModel.class;
    }

    @Override
    protected SheetAppDescBinding getViewBinding () {
        return binding;
    }

    @Override
    public void beforeView () {
        setCancelable(false);
    }

    @SuppressLint("CheckResult")
    @Override
    public void afterView () {
        viewModel.setNavigator(this);
        binding.setViewModel(viewModel);
        model = Arrays.asList(getActivity().getResources().getStringArray(R.array.appDesc));
        AppDesListAdapter adapter = new AppDesListAdapter();
        binding.rvDesc.setAdapter(adapter);
        adapter.setData(model);
    }


    @Override
    public void destroyView () {

    }
}