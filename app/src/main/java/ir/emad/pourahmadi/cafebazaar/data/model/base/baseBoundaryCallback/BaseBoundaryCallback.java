package ir.emad.pourahmadi.cafebazaar.data.model.base.baseBoundaryCallback;

import androidx.paging.PagedList;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesItems;
import ir.emad.pourahmadi.cafebazaar.data.network.ApiInterface;
import ir.emad.pourahmadi.cafebazaar.db.database.UserDao;
import ir.emad.pourahmadi.cafebazaar.db.database.VenuDao;

public class BaseBoundaryCallback extends PagedList.BoundaryCallback<VenuesItems> {
    ApiInterface apiService;
    VenuDao venuDao;
    int mPage;
    UserDao userDao;

    public BaseBoundaryCallback(VenuDao local,UserDao userDao, ApiInterface remote) {
        apiService = remote;
        this.userDao = userDao;
        venuDao = local;

        mPage = 0;
    }

    public void nextPage() {
        mPage++;
    }

    public void setPage(int page) {
        mPage = page;
    }
}