package ir.emad.pourahmadi.cafebazaar.data.model.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ir.emad.pourahmadi.cafebazaar.data.model.base.BaseResponse;


public class VenuesDetailModel extends BaseResponse {

    @Expose
    @SerializedName("response")
    private VenuesDetail response;

    public VenuesDetail getResponse () {
        return response;
    }

    public void setResponse (VenuesDetail response) {
        this.response = response;
    }


}
