package ir.emad.pourahmadi.cafebazaar.ui.features.fragments.detail;


import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesDetail;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesPhotos;

public interface DetailNavigator {

    void menuClick ();

    void response (VenuesDetail.Venue venue);

    void backClick ();

    void onContactClick ();

    void onShareClick ();

    void onDirectionClick ();

    void responsePhotos (VenuesPhotos.Photos photos);
}