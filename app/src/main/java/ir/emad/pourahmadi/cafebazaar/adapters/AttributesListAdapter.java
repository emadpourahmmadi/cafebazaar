package ir.emad.pourahmadi.cafebazaar.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import ir.emad.pourahmadi.cafebazaar.App;
import ir.emad.pourahmadi.cafebazaar.R;
import ir.emad.pourahmadi.cafebazaar.data.model.dataModel.VenuesDetail;
import ir.emad.pourahmadi.cafebazaar.databinding.ItemAttributeBinding;

public class AttributesListAdapter extends RecyclerView.Adapter <AttributesListAdapter.ContactViewHolder> {
    private final Animation doneAnim;
    String TAG = AttributesListAdapter.class.getSimpleName();
    private static final int VIEW_SELECT = 0;


    private List <VenuesDetail.Groups> list = new ArrayList <>();

    public AttributesListAdapter ( ) {
        doneAnim = AnimationUtils.loadAnimation(App.getINSTANCE(),
                android.R.anim.slide_out_right);
        doneAnim.setDuration(600);
    }

    public void setData (List <VenuesDetail.Groups> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount () {
        if (list == null)
            return 0;
        return list.size();
    }

    public void clear () {
        list.clear();
    }


    public int getItemViewType (int position) {
        return VIEW_SELECT;
    }


    @SuppressLint({"SetTextI18n", "ClickableViewAccessibility", "UseCompatLoadingForDrawables", "DefaultLocale"})
    @Override
    public void onBindViewHolder (@NonNull final ContactViewHolder holder, final int position) {
        if (list.get(position) != null) {
            VenuesDetail.Groups model = list.get(position);
            ItemAttributeBinding bindingSelected = holder.bindingSelected;
            bindingSelected.txtTitle.setText(model.getName() != null ? model.getName() : " - ");
        }


    }


    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder (@NonNull ViewGroup viewGroup, int viewType) {
        ViewDataBinding binding;

        switch (viewType) {
            case VIEW_SELECT:
                binding = DataBindingUtil.inflate(LayoutInflater.
                        from(viewGroup.getContext()), R.layout.item_attribute, viewGroup, false);
                return new ContactViewHolder((ItemAttributeBinding) binding);
        }
        return null;
    }


    public class ContactViewHolder extends RecyclerView.ViewHolder {

        private ItemAttributeBinding bindingSelected;

        ContactViewHolder (ItemAttributeBinding binding) {
            super(binding.getRoot());
            this.bindingSelected = binding;
        }
    }
}
