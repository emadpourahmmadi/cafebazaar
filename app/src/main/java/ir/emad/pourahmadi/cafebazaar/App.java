package ir.emad.pourahmadi.cafebazaar;


import androidx.multidex.MultiDexApplication;
import cat.ereza.customactivityoncrash.config.CaocConfig;
import dagger.hilt.android.HiltAndroidApp;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import ir.emad.pourahmadi.cafebazaar.di.RxBus;
import ir.emad.pourahmadi.cafebazaar.ui.features.activities.MainActivity.MainActivity;
import ir.emad.pourahmadi.cafebazaar.ui.features.activities.base.ForceCloseActivity;

@HiltAndroidApp
public class App extends MultiDexApplication {
    private static App INSTANCE;
    private static RxBus bus;

    public static RxBus bus () {
        return bus;
    }

    @Override
    public void onCreate () {
        super.onCreate();
        INSTANCE = this;
        bus = new RxBus();
        CaocConfig.Builder.create()
                .backgroundMode(CaocConfig.BACKGROUND_MODE_SILENT) //default: CaocConfig.BACKGROUND_MODE_SHOW_CUSTOM
                .enabled(true) //default: true
                .showErrorDetails(false) //default: true
                .showRestartButton(true) //default: true
                .trackActivities(true) //default: false
                .minTimeBetweenCrashesMs(3000) //default: 3000
                .errorDrawable(R.drawable.error) //default: bug image
                .restartActivity(MainActivity.class) //default: null (your app's launch activity)
                .errorActivity(ForceCloseActivity.class) //default: null (default error activity)
                //.eventListener(new YourCustomEventListener()) //default: null
                .apply();

        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/IRANSans.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());
    }

    public static App getINSTANCE () {
        return INSTANCE;
    }
}
