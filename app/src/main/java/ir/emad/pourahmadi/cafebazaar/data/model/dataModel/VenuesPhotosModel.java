package ir.emad.pourahmadi.cafebazaar.data.model.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ir.emad.pourahmadi.cafebazaar.data.model.base.BaseResponse;


public class VenuesPhotosModel extends BaseResponse {

    @Expose
    @SerializedName("response")
    private VenuesPhotos response;

    public VenuesPhotos getResponse () {
        return response;
    }

    public void setResponse (VenuesPhotos response) {
        this.response = response;
    }


}
