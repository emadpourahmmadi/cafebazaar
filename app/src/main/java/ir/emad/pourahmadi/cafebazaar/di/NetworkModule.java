package ir.emad.pourahmadi.cafebazaar.di;


import android.app.Application;
import android.content.Intent;
import android.content.pm.PackageManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import androidx.room.Room;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import ir.emad.pourahmadi.cafebazaar.App;
import ir.emad.pourahmadi.cafebazaar.BuildConfig;
import ir.emad.pourahmadi.cafebazaar.R;
import ir.emad.pourahmadi.cafebazaar.data.network.ApiInterface;
import ir.emad.pourahmadi.cafebazaar.db.database.AppDatabase;
import ir.emad.pourahmadi.cafebazaar.db.database.UserDao;
import ir.emad.pourahmadi.cafebazaar.db.database.VenuDao;
import ir.emad.pourahmadi.cafebazaar.utils.Constant;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@Module
@InstallIn(ApplicationComponent.class)
public class NetworkModule {
    private static final int CONNECT_TIMEOUT_IN_MS = 60000;

    @Provides
    @Singleton
    RequestManager requestManager (Application application, RequestOptions requestOptions) {
        return Glide.with(application).applyDefaultRequestOptions(requestOptions);
    }

    @Provides
    @Singleton
    RequestOptions requestOptions () {
        return new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.placeholder);
    }

    @Provides
    @Singleton
    Cache provideOkHttpCache (Application application) {
        int cacheSize = 10 * 1024 * 1024;
        return new Cache(application.getCacheDir(), cacheSize);
    }

    @Provides
    @Singleton
    Gson gson () {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient (Cache cache, UserDao database) {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT_IN_MS, TimeUnit.MILLISECONDS)
                .readTimeout(CONNECT_TIMEOUT_IN_MS, TimeUnit.MILLISECONDS)
                .writeTimeout(CONNECT_TIMEOUT_IN_MS, TimeUnit.MILLISECONDS)
                .addInterceptor(loggingInterceptor)
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    Request request;


                    if (database.getUserData() != null) {
                        request = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .header("accept-charset", "UTF-8")
                                .build();

                    } else {
                        request = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .header("accept-charset", "UTF-8")
                                .build();

                    }


                    Response response = chain.proceed(request);
                    response.cacheResponse();


                    switch (response.code()) {
                        case 401:
                            database.clearData();
                            PackageManager pm = App.getINSTANCE().getPackageManager();
                            Intent intent = pm.getLaunchIntentForPackage(BuildConfig.APPLICATION_ID);
                            App.getINSTANCE().startActivity(intent);
                            break;
                        case 403:
                            break;
                    }

                    return response;
                })
                .cache(cache)
                .retryOnConnectionFailure(true)
                .build();
    }

    @Provides
    @Singleton
    AppDatabase database (Application application) {
        return Room.databaseBuilder(application,
                AppDatabase.class, Constant.DataBaseName)
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
    }

    @Provides
    @Singleton
    UserDao userDao (AppDatabase database) {
        return database.userDao();
    }
    @Provides
    @Singleton
    VenuDao venuDao (AppDatabase database) {
        return database.venuDao();
    }

    @Provides
    @Singleton
    public static ApiInterface providerApiService (Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASEURL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
                .create(ApiInterface.class);
    }


}
